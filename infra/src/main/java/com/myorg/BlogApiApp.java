package com.myorg;

import software.amazon.awscdk.App;
import software.amazon.awscdk.Environment;
import software.amazon.awscdk.StackProps;

public class BlogApiApp {
    public static void main(final String[] args) {
        App app = new App();

        new BlogApiStack(app, "BlogApiStack", StackProps.builder()
                .description("The aws stack which will be used to host the blog api backend")
                .analyticsReporting(true)
                .terminationProtection(true)
                .env(Environment.builder()
                        .account(System.getenv("CDK_DEFAULT_ACCOUNT"))
                        .region(System.getenv("CDK_DEFAULT_REGION"))
                        .build())
                .build());

        app.synth();
    }
}

