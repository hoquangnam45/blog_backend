package com.myorg;

import software.amazon.awscdk.Duration;
import software.amazon.awscdk.services.s3.Bucket;
import software.amazon.awscdk.services.ses.actions.S3;
import software.amazon.awscdk.services.sqs.Queue;
import software.constructs.Construct;
import software.amazon.awscdk.Stack;
import software.amazon.awscdk.StackProps;
// import software.amazon.awscdk.Duration;
// import software.amazon.awscdk.services.sqs.Queue;

public class BlogApiStack extends software.amazon.awscdk.StackStack {
    public BlogApiStack(final Construct scope, final String id) {
        this(scope, id, null);
    }

    public BlogApiStack(final Construct scope, final String id, final StackProps props) {
        super(scope, id, props);

        // The code that defines your stack goes here
        var pipeline;
        var vpc;
        var subnet;
        var securitygroup;
        var nodegroup;
        var ec2instance;
        var deployment;
        var loadBalancer;
        var elasticIp;
        var persistentVolume;
        var publicPersistentVolume(s3 bucket)
        var ecr;
        var storedArtifact;
        var kubernetesCluster;
        var iam;

        // How anything goes
//      After the CI/CD of gitlab run it will create an image and push that image to the gitlab registry
//      after pushing it will use aws webhook to trigger aws pipeline to do the followings:
//          + Pull the newly created image
//          + Deploy it to the ec2 instance
//      Public assets will exist inside s3 bucket, all upload to public blog will exist here
//      The step to figure what would be need to be provisioned inside this application
//              + Step 1: Manually provision myself and see what would be needed
//              + Step 2: Convert those to a template with aws cdk
//      * What could be the problems for this approach:
//            + Manually handle fail-over
//            + Manually handle backup
//            + Manually handle security
//      What architecture to go with here: Microservices
//      What service would be needed:
//                + Search
//                + Authentication
//                + General-purpose
//                + Handle batch-job processing
//      Questions: How do I automate all of this? (AWS CDK)
//                      fe              -> public persistent volume (assets, user uploaded files) (s3)
//      nginx -> ELB -> api
//                   -> db              -> persistent_volume (s3)
//                   -> cache                ^
//                   -> search (index)       ^
//                   -> message queue (Have not had any purpose yet)
//                   -> handle transaction service
//
//        How is the plan to achieve all of this:
//          + Provision all the resource needed by hand first
//                    How to provision the resource needed
//                              + Use iam to create a role for eks cluster
//                              + Create eks cluster
//                              + Create kubernetes node (ec2 instance)
//                              + Add the node to eks cluster (managed node group or self-managed)
//                              + Create persistent volume (s3)
//                              + Attach the persistent volume to ec2 instance

        var bucket = Bucket.Builder.create(scope, "BlogApiBucket").build();
        var queue = Queue.Builder.create(this, "InfraQueue")
                 .visibilityTimeout(Duration.seconds(300))
                 .build();

    }
}
