openapi: 3.0.0
info:
  title: Blog Backend API
  description: Blog Backend API
  version: 0.0.1-SNAPSHOT
tags:
  - name: Comment
  - name: User
  - name: Blog
  - name: Authenticate
  - name: Tag
paths:
  /register:
    post:
      tags:
        - Authenticate
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/RegisterRequest'
      responses:
        200:
          description: 'Operation success'
        409:
          description: 'The email or usename had been used to register with the system'
        default:
          $ref: '#/components/responses/UnexpectedErrorException'
      security: []
  /login:
    post:
      tags:
        - Authenticate
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/LoginRequest'
      responses:
        200:
          description: >
            Successfully authenticated.
            The session ID is returned in a cookie named `JSESSIONID`. You need to include this cookie in subsequent requests.
          headers:
            Set-Cookie:
              schema:
                type: string
                example: JSESSIONID=abcde12345; Path=/; HttpOnly
        404:
          description:
          A user with the specified ID was not found.
        default:
          $ref: '#/components/responses/UnexpectedErrorException'
      security: []
  /logout:
    post:
      tags:
        - Authenticate
      responses:
        200:
          description: Logout successfully
        default:
          $ref: '#/components/responses/UnexpectedErrorException'
  /comment:
    get:
      tags:
        - Comment
      parameters:
        - in: term
          name: blogId
          schema:
            type: integer
            format: int64
      responses:
        200:
          description: Get all comments on blog
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/CommentDTO'
    post:
      tags:
        - Comment
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreateCommentRequest'
      responses:
        200:
          $ref: '#/components/schemas/CommentDTO'
        401:
          $ref: '#/components/responses/UnauthorizedException'
        default:
          $ref: '#/components/responses/UnexpectedErrorException'
  /comment/{commentId}:
    parameters:
      - in: path
        name: commentId
        schema:
          type: integer
          format: int64
        required: true
    put:
      tags:
        - Comment
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CommentDTO'
      responses:
        200:
          $ref: '#/components/schemas/CommentDTO'
        404:
          $ref: '#/components/responses/ResourceNotFoundException'
        401:
          $ref: '#/components/responses/UnauthorizedException'
        default:
          $ref: '#/components/responses/UnexpectedErrorException'
    delete:
      tags:
        - Comment
      responses:
        200:
          description: 'OK'
        404:
          $ref: '#/components/responses/ResourceNotFoundException'
        401:
          $ref: '#/components/responses/UnauthorizedException'
        default:
          $ref: '#/components/responses/UnexpectedErrorException'
  /tag:
    post:
      tags:
        - Tag
      description: Create new tag
      requestBody:
        content:
          text/plain:
            schema:
              type: string
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/TagDTO'
        409:
          description: Create an already existed tag
          content:
            application/json:
              schema:
                $ref: '#/components/responses/ResourceConflictException'
        default:
          $ref: '#/components/responses/UnexpectedErrorException'
  /tag/popular:
    get:
      tags:
        - Tag
      description: Getting the trending tags
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/TagDTO'
        default:
          $ref: '#/components/responses/UnexpectedErrorException'
  /tag/search:
    get:
      tags:
        - Tag
      description: Searching the tags
      parameters:
        - in: term
          name: term
          schema:
            type: string
      responses:
        200:
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/TagDTO'
        default:
          $ref: '#/components/responses/UnexpectedErrorException'
  /tag/{tagName}:
    parameters:
      - in: path
        name: tagName
        schema:
          type: string
    delete:
      tags:
        - Tag
      responses:
        200:
          description: OK
        default:
          $ref: '#/components/responses/UnexpectedErrorException'
components:
  responses:
    UnexpectedErrorException:
      description: 'Unexpected error'
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/MessageResponse'
    UnauthorizedException:
      description: 'The user dont have the permission to do the operation'
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/MessageResponse'
    ResourceNotFoundException:
      description: 'The resource could not be found'
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/MessageResponse'
    ResourceConflictException:
      description: 'Try to create an already existed resource, or the operation can introduce a conflict in the resource'
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/MessageResponse'
  schemas:
    Response:
      type: object
      properties:
        statusCode:
          type: number
        success:
          type: boolean
        payload:
          type: object
    RegisterRequest:
      type: object
      properties:
        username:
          type: string
        password:
          type: string
          format: password
        email:
          type: string
          format: email
      required:
        - username
        - password
        - email
    LoginRequest:
      type: object
      properties:
        username:
          type: string
        password:
          type: string
      required:
        - username
        - password
    UserDTO:
      type: object
      properties:
        id:
          type: integer
          format: int64
        username:
          type: string
        email:
          type: string
          format: email
    TagDTO:
      type: object
      properties:
        id:
          type: integer
          format: int64
        keyboard:
          type: string
    BlogDTO:
      type: object
      properties:
        id:
          type: integer
        authoredBy:
          type: string
        modified:
          type: boolean
        content:
          type: string
        title:
          type: string
        createdDate:
          type: string
          format: datetime
        lastModifiedDate:
          type: string
          format: datetime
        tags:
          type: array
          items:
            $ref: '#/components/schemas/TagDTO'
    CreateCommentRequest:
      type: object
      properties:
        blogId:
          type: integer
          format: int64
        replyId:
          type: integer
          format: int64
        userId:
          type: integer
          format: int64
        content:
          type: string
    CommentDTO:
      type: object
      properties:
        id:
          type: integer
          format: int64
        blogId:
          type: integer
          format: int64
        replyId:
          type: integer
          format: int64
        user:
          $ref: '#/components/schemas/UserDTO'
        content:
          type: string
        createdDate:
          type: string
          format: datetime
        modified:
          type: boolean
        lastModifiedDate:
          type: string
          format: datetime
        numberOfReply:
          type: integer
    MessageResponse:
      type: object
      properties:
        message:
          type: string
  securitySchemes:
    BasicAuth:
      type: http
      scheme: basic
    CookieAuth:
      description: Authentication with session key
      type: apiKey
      in: cookie
      name: JSESSIONID
security:
  - BasicAuth: []
  - CookieAuth: []
