package com.hoquangnam.blogbackend.controller;

import com.hoquangnam.blogbackend.core.exception.ResourceNotFoundException;
import com.hoquangnam.blogbackend.core.exception.UnauthorizedException;
import com.hoquangnam.blogbackend.model.dto.CommentDTO;
import com.hoquangnam.blogbackend.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Set;
import java.util.UUID;

@RestController
@RequestMapping("/api/comment")
public class CommentController {
    private final CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @GetMapping
    private Set<CommentDTO> getCommentOnBlog(@RequestParam(name = "blogId") UUID blogId) throws ResourceNotFoundException {
        return commentService.getCommentsOnBlog(blogId);
    }

    @PostMapping
    private CommentDTO createComment(Authentication authentication, @Valid @RequestBody CommentDTO request) throws ResourceNotFoundException {
        return commentService.createComment(authentication, request);
    }

    @PutMapping("/{commentId}")
    private CommentDTO editComment(
            Authentication authentication,
            @Valid @RequestBody CommentDTO commentDTO,
            @PathVariable(name = "commentId") UUID commentId)
            throws ResourceNotFoundException, UnauthorizedException {
        return commentService.editComment(authentication, commentId, commentDTO);
    }

    @DeleteMapping("/{commentId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    private void deleteComment(
            Authentication authentication,
            @PathVariable("commentId") UUID commentId)
            throws ResourceNotFoundException, UnauthorizedException {
        commentService.deleteComment(authentication, commentId);
    }
}