package com.hoquangnam.blogbackend.controller;

import com.hoquangnam.blogbackend.core.exception.ResourceConflictException;
import com.hoquangnam.blogbackend.core.exception.ResourceNotFoundException;
import com.hoquangnam.blogbackend.model.dto.TagDTO;
import com.hoquangnam.blogbackend.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/tag")
public class TagController {
    private final TagService tagService;
    private final Integer topTrendingNumber;

    @Autowired
    public TagController(TagService tagService,
                         @Value("${app.tag.topTrendingNumber}") Integer topTrendingNumber) {
        this.tagService = tagService;
        this.topTrendingNumber = topTrendingNumber;
    }

    @PostMapping
    public TagDTO createTag(@RequestBody String tag) throws ResourceConflictException {
        return tagService.createTag(tag);
    }

    @DeleteMapping("/{tagName}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTag(@PathVariable(name = "tagName") String tagName) throws ResourceNotFoundException {
        tagService.deleteTag(tagName);
    }

    @GetMapping("/popular")
    public List<TagDTO> getTrendingTag() {
        return tagService.getTrendingTag(topTrendingNumber);
    }

    @GetMapping("/search")
    public List<TagDTO> searchTag(
            @RequestParam(name = "term") String searchTerm,
            @RequestParam(name = "limit") Integer limit) {
        return tagService.searchTag(searchTerm, PageRequest.of(0, limit)).getContent();
    }
}