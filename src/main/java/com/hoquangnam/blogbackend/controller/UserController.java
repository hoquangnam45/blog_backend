package com.hoquangnam.blogbackend.controller;

import com.hoquangnam.blogbackend.core.exception.UserNotFoundException;
import com.hoquangnam.blogbackend.model.dto.FollowRequest;
import com.hoquangnam.blogbackend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/user")
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/follow")
    public void follow(Authentication authentication, @Valid @RequestBody FollowRequest request) throws UserNotFoundException {
        userService.follow(authentication, request.getFollowingId());
    }

    @PostMapping("/unfollow")
    public void unfollow(Authentication authentication, @Valid @RequestBody FollowRequest request) throws UserNotFoundException {
        userService.unfollow(authentication, request.getFollowingId());
    }
}