package com.hoquangnam.blogbackend.controller;

import com.hoquangnam.blogbackend.core.exception.AlreadyLoginException;
import com.hoquangnam.blogbackend.core.exception.AuthException;
import com.hoquangnam.blogbackend.core.exception.BadRequestException;
import com.hoquangnam.blogbackend.core.exception.ResourceConflictException;
import com.hoquangnam.blogbackend.core.security.service.TokenService;
import com.hoquangnam.blogbackend.model.dto.AuthDTO;
import com.hoquangnam.blogbackend.model.dto.TokenRequestDTO;
import com.hoquangnam.blogbackend.model.dto.TokenResponseDTO;
import com.hoquangnam.blogbackend.service.AuthService;
import com.hoquangnam.blogbackend.service.SecurityContextService;
import com.hoquangnam.blogbackend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    private final UserService userService;
    private final AuthService authService;
    private final TokenService tokenService;
    private final SecurityContextService securityContextService;

    @Autowired
    public AuthController(
            UserService userService,
            AuthService authService,
            TokenService tokenService,
            SecurityContextService securityContextService) {
        this.userService = userService;
        this.authService = authService;
        this.tokenService = tokenService;
        this.securityContextService = securityContextService;
    }

    @PostMapping("/login")
    public TokenResponseDTO authenticateUser(@Validated @RequestBody AuthDTO authDTO) throws AuthException, AlreadyLoginException {
        if (securityContextService.isAuthenticated()) {
            throw new AlreadyLoginException("User had already login no further action is needed");
        }
        return authService.login(authDTO.getUsername(), authDTO.getPassword());
    }

    @PostMapping("/register")
    public TokenResponseDTO registerUser(@Validated @RequestBody AuthDTO request) throws ResourceConflictException, AuthException {
        userService.createUser(request);
        return authService.login(request.getUsername(), request.getPassword());
    }

    @PostMapping("/refresh")
    public TokenResponseDTO refreshToken(@Validated @RequestBody TokenRequestDTO request) throws BadRequestException {
        return tokenService.refreshToken(request);
    }

    @PostMapping("/logout")
    public void logoutUser(HttpServletRequest request) throws BadRequestException {
        var accessToken = tokenService.getAccessToken(request).orElse(null);
        authService.logout(accessToken);
    }
}