package com.hoquangnam.blogbackend.controller;

import co.elastic.clients.elasticsearch._types.SortOrder;
import co.elastic.clients.elasticsearch.core.search.Hit;
import com.hoquangnam.blogbackend.core.exception.ResourceNotFoundException;
import com.hoquangnam.blogbackend.core.exception.UnauthorizedException;
import com.hoquangnam.blogbackend.core.search.document.BlogDocument;
import com.hoquangnam.blogbackend.core.search.search.filter.BlogFilter;
import com.hoquangnam.blogbackend.core.search.search.request.BlogSearchRequestGenerator;
import com.hoquangnam.blogbackend.model.dto.BlogDTO;
import com.hoquangnam.blogbackend.service.BlogService;
import com.hoquangnam.blogbackend.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.io.IOException;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/blog")
public class BlogController {
    private final BlogService blogService;
    private final SearchService searchService;

    @Autowired
    public BlogController(
            BlogService blogService,
            SearchService searchService) {
        this.blogService = blogService;
        this.searchService = searchService;
    }

    @PostMapping
    public BlogDTO createNewBlog(Authentication authentication, @RequestBody BlogDTO request) {
        return blogService.createBlog(authentication, request);
    }

    @GetMapping("/{blogId}")
    public BlogDTO getBlog(@PathVariable UUID blogId) throws ResourceNotFoundException {
        return blogService.getBlog(blogId);
    }

    @DeleteMapping("/{blogId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteBlogs(Authentication authentication, @PathVariable UUID blogId) throws ResourceNotFoundException, UnauthorizedException {
        blogService.deleteBlog(authentication, blogId);
    }

    @PutMapping("/{blogId}")
    public BlogDTO updateBlog(Authentication authentication, @Valid @RequestBody BlogDTO blogDTO, @PathVariable UUID blogId) throws ResourceNotFoundException, UnauthorizedException {
        return blogService.updateBlog(authentication, blogId, blogDTO);
    }

    @GetMapping("/search")
    public List<BlogDocument> searchBlog(@RequestParam(name = "tag", required = false) String tag,
                                         @RequestParam(name = "title", required = false) String title,
                                         @RequestParam(name = "content", required = false) String content,
                                         @RequestParam(name = "author", required = false) String author,
                                         @RequestParam(name = "from", required = false) Integer from,
                                         @RequestParam(name = "size", required = false) Integer size,
                                         @RequestParam(name = "createdDateFrom", required = false) Long createdDateFrom,
                                         @RequestParam(name = "createdDateTo", required = false) Long createdDateTo,
                                         @RequestParam(name = "highlightField", required = false) String highlightField,
                                         @RequestParam(name = "rating", required = false) Integer rating,
                                         @RequestParam(name = "sortOrder", required = false) SortOrder sortOrder,
                                         @RequestParam(name = "sortField", required = false) String sortField) throws IOException {
        var filter = BlogFilter.builder()
                .tag(tag)
                .title(title)
                .author(author)
                .from(from)
                .size(size)
                .content(content)
                .createdDateFrom(Optional.ofNullable(createdDateFrom).map(Instant::ofEpochSecond).map(OffsetDateTime::from).orElse(null))
                .createdDateTo(Optional.ofNullable(createdDateTo).map(Instant::ofEpochSecond).map(OffsetDateTime::from).orElse(null))
                .rating(rating)
                .sortField(sortField)
                .sortOrder(sortOrder)
                .highlightField(highlightField)
                .build();
        var searchRequest = BlogSearchRequestGenerator.generateSearchRequest(filter);
        return searchService.search(searchRequest, BlogDocument.class).hits().hits().stream().map(Hit::source).collect(Collectors.toList());
    }
}