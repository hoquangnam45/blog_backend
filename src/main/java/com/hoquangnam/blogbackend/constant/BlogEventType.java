package com.hoquangnam.blogbackend.constant;

public enum BlogEventType {
    CREATED,
    UPDATED,
    DELETED
}