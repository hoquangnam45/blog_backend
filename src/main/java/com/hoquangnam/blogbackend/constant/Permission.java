package com.hoquangnam.blogbackend.constant;

public enum Permission {
    CREATE,
    READ,
    UPDATE,
    DELETE
}