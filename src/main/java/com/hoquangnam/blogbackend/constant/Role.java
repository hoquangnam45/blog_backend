package com.hoquangnam.blogbackend.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Optional;
import java.util.stream.Stream;

@Getter
@AllArgsConstructor
public enum Role {
    ADMIN(0, "ADMIN"), BLOGGER(1, "BLOGGER");

    private final int id;
    private final String name;

    public static Role fromValue(Integer val) {
        return Optional.ofNullable(val)
                .flatMap(it ->
                        Stream.of(Role.values())
                            .filter(role -> role.getId() == it)
                            .findFirst())
                .orElse(null);
    }
}
