package com.hoquangnam.blogbackend.config;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.client.RestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ElasticConfig {
    private final String elasticHost;
    private final int elasticPort;
    private final String elasticPassword;
    private final String elasticUsername;
    private final ObjectMapper objectMapper;

    @Autowired
    public ElasticConfig(
            @Value("${elastic.host}") String elasticHost,
            @Value("${elastic.port}") int elasticPort,
            @Value("${elastic.password}") String elasticPassword,
            @Value("${elastic.username}") String elasticUsername,
            ObjectMapper objectMapper // Use object mapper from spring, it already register some basic module like jsr310
            ) {
        this.elasticHost = elasticHost;
         this.elasticPort = elasticPort;
        this.elasticPassword = elasticPassword;
        this.elasticUsername = elasticUsername;
        this.objectMapper = objectMapper;
    }

    @Bean
    ElasticsearchClient elasticSearchClient() {
        var credentialProvider = new BasicCredentialsProvider();
        credentialProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(elasticUsername, elasticPassword));
        var httpClient = RestClient.builder(new HttpHost(elasticHost, elasticPort))
                .setHttpClientConfigCallback(httpAsyncClientBuilder -> httpAsyncClientBuilder.setDefaultCredentialsProvider(credentialProvider))
                .build();
        var jackson = new JacksonJsonpMapper(objectMapper);
        var transport = new RestClientTransport(httpClient, jackson);
        return new ElasticsearchClient(transport);
    }
}
