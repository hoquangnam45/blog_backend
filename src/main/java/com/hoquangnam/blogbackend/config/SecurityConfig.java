package com.hoquangnam.blogbackend.config;

import com.hoquangnam.blogbackend.core.security.filter.AuthTokenFilter;
import com.hoquangnam.blogbackend.core.security.providers.jwt.JwtAuthenticationProvider;
import com.hoquangnam.blogbackend.core.security.service.TokenService;
import com.hoquangnam.blogbackend.service.AuthService;
import com.hoquangnam.blogbackend.service.SecurityContextService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.ExceptionTranslationFilter;

/**
 * Custom filter should not be a bean (annotated with @Component or created via @Bean) since spring security
 * will auto register it bypassing the config
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig {
    public final static String[] publicApis = {
            "/api/auth/register",
            "/api/auth/login",
            "/api/auth/refresh",
            "/api/blogs/search",
            "swagger-ui/**"
    };

    public final static String[] publicGetApis = {
            "/api/blog/**"
    };

    @Bean
    public DaoAuthenticationProvider configuredDaoAuthenticationProvider(PasswordEncoder passwordEncoder, UserDetailsService userDetailsService) {
        var provider = new DaoAuthenticationProvider();
        provider.setPasswordEncoder(passwordEncoder);
        provider.setUserDetailsService(userDetailsService);
        return provider;
    }

    @Bean
    public AuthenticationManager configureAuthenticationManager(HttpSecurity http, DaoAuthenticationProvider daoAuthenticationProvider, JwtAuthenticationProvider jwtAuthenticationProvider) throws Exception {
        return http.getSharedObject(AuthenticationManagerBuilder.class)
                .authenticationProvider(jwtAuthenticationProvider)
                .authenticationProvider(daoAuthenticationProvider)
                .build();
    }

    @Bean
    public PasswordEncoder configuredPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * When the request come, it's always pass through all the filter to build the security context authentication,
     * there's a special filter called AnonymousAuthenticationFilter which will always
     * build a special security context authentication (AnonymousAuthentication) if there's no filter handle the request,
     * for api which required to be authenticated if this AnonymousAuthentication is present, it will trigger the authentication
     * exception but for permitAll api, it will not trigger the exception
     */
    @Bean
    public SecurityFilterChain configuredSecurityFilterChain(
            HttpSecurity http,
            AuthenticationEntryPoint unauthorizedHandler,
            AuthService authService,
            SecurityContextService securityContextService,
            TokenService tokenService) throws Exception {
        var authTokenFilter = new AuthTokenFilter(authService, securityContextService, tokenService);
        http.cors().and().csrf().disable()
                .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests()
//                .antMatchers("/api/auth/register").permitAll()
//                .antMatchers("/api/auth/login").permitAll()
//                .antMatchers("/api/blogs/search").permitAll()
//                .antMatchers("swagger-ui/**").authenticated()
                .anyRequest().authenticated()
                .and()
                .addFilterAfter(authTokenFilter, ExceptionTranslationFilter.class);
        return http.build();
    }

    @Bean
    public WebSecurityCustomizer configuredWebSecurity() {
        return (webSecurity) -> webSecurity.ignoring()
                .antMatchers(HttpMethod.GET, publicGetApis)
                .antMatchers(publicApis);
    }
}