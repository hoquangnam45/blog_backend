package com.hoquangnam.blogbackend.model.persistence;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.time.OffsetDateTime;
import java.util.UUID;
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@MappedSuperclass
public abstract class BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", insertable = false, updatable = false)
    protected UUID id;
    
    @Column(name="createdDate", updatable = false)
    @CreationTimestamp
    protected OffsetDateTime createdDate;
    
    @Column(name="updatedDate", insertable = false)
    @UpdateTimestamp
    protected OffsetDateTime updatedDate;
    
    @Column(name="deletedDate", insertable = false)
    protected OffsetDateTime deletedDate;

    @Column(name="lastModifiedDate")
    @UpdateTimestamp
    protected OffsetDateTime lastModifiedDate;

    public void softDelete() {
        var now = OffsetDateTime.now();
        deletedDate = now;
        lastModifiedDate = now;
    }
}