package com.hoquangnam.blogbackend.model.persistence;

import com.hoquangnam.blogbackend.core.security.permission.HaveAuthor;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Optional;
import java.util.UUID;

@Getter
@Setter
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name="comment_active")
public class CommentEntity extends BaseEntity implements Serializable, HaveAuthor {
    private static final long serialVersionUID = 8886386298544143294L;

    @Column(name="content")
    private String content;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="blogId", referencedColumnName = "id", updatable = false)
    private BlogEntity blog;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="userId", referencedColumnName = "id", updatable = false)
    private UserEntity user;

    // This should be use new table to be better
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinTable(name = "reply", joinColumns = @JoinColumn(name = "commentId", updatable = false), inverseJoinColumns = @JoinColumn(name="replyTo", updatable = false))
    private CommentEntity replyTo;

    @Override
    public String getAuthorId() {
        return Optional.ofNullable(user)
                .map(UserEntity::getId)
                .map(UUID::toString)
                .orElse(null);
    }
}