package com.hoquangnam.blogbackend.model.persistence;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.Set;

@Getter
@Setter
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name="blog_user_active")
public class UserEntity extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1127250200236060317L;

    @Column(name="username", unique = true, updatable = false)
    private String username;

    @Column(name="password")
    private String password;

    @Column(name="email", unique=true)
    private String email;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_role",
            joinColumns = @JoinColumn(name = "userId"),
            inverseJoinColumns = @JoinColumn(name = "roleId"))
    private Set<RoleEntity> roles;

    @Column(name="activatedDate")
    private OffsetDateTime activatedDate;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "following",
            joinColumns = @JoinColumn(name = "userId"),
            inverseJoinColumns = @JoinColumn(name = "followerId")
    )
    private Set<UserEntity> followers;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "history",
            joinColumns = @JoinColumn(name ="userId"),
            inverseJoinColumns = @JoinColumn(name = "blogId"))
    private Set<BlogEntity> readBlogs;
}