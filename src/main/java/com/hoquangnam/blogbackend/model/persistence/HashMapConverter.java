package com.hoquangnam.blogbackend.model.persistence;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.io.IOException;
import java.util.Map;

@Component
@Converter
public class HashMapConverter implements AttributeConverter<Map<String, ?>, String> {
    private final ObjectMapper objectMapper;
    private final Logger logger = LoggerFactory.getLogger(HashMapConverter.class);

    public HashMapConverter(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public String convertToDatabaseColumn(Map<String, ?> attribute) {
        try {
            return objectMapper.writeValueAsString(attribute);
        } catch (JsonProcessingException e) {
            logger.error("Convert object into db column have error", e);
            return null;
        }
    }

    @Override
    public Map<String, ?> convertToEntityAttribute(String dbData) {
        try {
            return objectMapper.readValue(dbData, new TypeReference<>() {});
        } catch (IOException e) {
            logger.error("Parse json into entity have error", e);
            return null;
        }
    }
}