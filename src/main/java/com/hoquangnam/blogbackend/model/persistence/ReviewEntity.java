package com.hoquangnam.blogbackend.model.persistence;

import com.hoquangnam.blogbackend.model.persistence.ReviewId;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name="review")
public class ReviewEntity implements Serializable {
    private static final long serialVersionUID = 6404247143732149179L;

    @EmbeddedId
    private ReviewId reviewId;

    @JoinColumn
    @Column(name="rating")
    private int rating;
}