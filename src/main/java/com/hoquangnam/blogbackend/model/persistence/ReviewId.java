package com.hoquangnam.blogbackend.model.persistence;

import com.hoquangnam.blogbackend.model.persistence.BlogEntity;
import com.hoquangnam.blogbackend.model.persistence.UserEntity;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class ReviewId implements Serializable {
    @ManyToOne
    @JoinColumn(name="blogId", referencedColumnName = "id", updatable = false)
    private BlogEntity blog;

    @ManyToOne
    @JoinColumn(name = "userId", referencedColumnName = "id", updatable = false)
    private UserEntity user;
}