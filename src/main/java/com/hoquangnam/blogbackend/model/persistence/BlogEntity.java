package com.hoquangnam.blogbackend.model.persistence;

import com.hoquangnam.blogbackend.core.security.permission.HaveAuthor;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name="blog_active")
public class BlogEntity extends BaseEntity implements Serializable, HaveAuthor {
    private static final long serialVersionUID = -7563184063803925336L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="authoredBy", referencedColumnName = "id", updatable = false)
    private UserEntity authoredBy;

    @Column(name="content")
    private String content;

    @Column(name="title")
    private String title;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "blog_tag", joinColumns = @JoinColumn(name = "blogId", updatable = false), inverseJoinColumns = @JoinColumn(name = "tagId", insertable = false, updatable = false))
    private Set<TagEntity> tags;

    @Override
    public String getAuthorId() {
        return Optional.ofNullable(authoredBy)
                .map(UserEntity::getId)
                .map(UUID::toString)
                .orElse(null);
    }
}