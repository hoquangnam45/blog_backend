package com.hoquangnam.blogbackend.model.persistence;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.Set;

@Getter
@Setter
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name="tag_active")
public class TagEntity extends BaseEntity {
    @Column(name="keyword", unique = true, updatable = false)
    private String keyword;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "tags")
    private Set<BlogEntity> blogs;
}