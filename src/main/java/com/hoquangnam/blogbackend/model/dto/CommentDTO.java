package com.hoquangnam.blogbackend.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;
import java.util.UUID;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class CommentDTO {
    private UUID id;
    @NotNull
    private UUID blogId;
    @Nullable
    private UUID replyId;
    @NotNull
    private UserDTO user;
    private OffsetDateTime createdDate;
    private OffsetDateTime updatedDate;
    private String content;
}
