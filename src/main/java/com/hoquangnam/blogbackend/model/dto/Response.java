package com.hoquangnam.blogbackend.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@Builder
@NoArgsConstructor
@EqualsAndHashCode
public class Response<T> {
    @Builder.Default
    private int statusCode = 200;
    @Builder.Default
    private boolean success = true;
    private T payload;

    public static <E> Response<E> from(E payload) {
        return Response.<E>builder().payload(payload).build();
    }

    public static Response<Void> empty() {
        return Response.from(null);
    }
}
