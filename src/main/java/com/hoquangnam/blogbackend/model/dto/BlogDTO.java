package com.hoquangnam.blogbackend.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
public class BlogDTO {
    private UUID id;
    private UserDTO authoredBy;
    private String title;
    private String content;
    private OffsetDateTime createdDate;
    private OffsetDateTime updatedDate;
    private Set<TagDTO> tags;
}
