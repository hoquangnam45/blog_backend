package com.hoquangnam.blogbackend.model.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
public class AuthDTO {
    private String username;
    private String password;
    private String email;
}