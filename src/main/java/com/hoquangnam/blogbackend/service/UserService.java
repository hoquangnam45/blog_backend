package com.hoquangnam.blogbackend.service;

import com.hoquangnam.blogbackend.core.exception.ResourceConflictException;
import com.hoquangnam.blogbackend.core.exception.UserNotFoundException;
import com.hoquangnam.blogbackend.model.dto.AuthDTO;
import com.hoquangnam.blogbackend.model.dto.UserDTO;
import org.springframework.security.core.Authentication;

import java.util.UUID;

public interface UserService {
    void follow(Authentication authentication, UUID followingId) throws UserNotFoundException;
    void unfollow(Authentication authentication, UUID followingId) throws UserNotFoundException;
    UserDTO createUser(AuthDTO request) throws ResourceConflictException;
}