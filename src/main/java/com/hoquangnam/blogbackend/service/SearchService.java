package com.hoquangnam.blogbackend.service;

import co.elastic.clients.elasticsearch.core.SearchRequest;
import co.elastic.clients.elasticsearch.core.SearchResponse;

import java.io.IOException;

public interface SearchService {
    <T> SearchResponse<T> search(SearchRequest searchRequest, Class<T> documentClazz) throws IOException;
}
