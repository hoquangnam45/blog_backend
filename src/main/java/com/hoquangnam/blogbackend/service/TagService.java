package com.hoquangnam.blogbackend.service;

import com.hoquangnam.blogbackend.core.exception.ResourceConflictException;
import com.hoquangnam.blogbackend.core.exception.ResourceNotFoundException;
import com.hoquangnam.blogbackend.model.dto.TagDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface TagService {
    void deleteTag(String tag) throws ResourceNotFoundException;
    TagDTO createTag(String tag) throws ResourceConflictException;
    List<TagDTO> getTrendingTag(Integer topTrendingNumber);
    Page<TagDTO> searchTag(String keyword, PageRequest pageRequest);
}