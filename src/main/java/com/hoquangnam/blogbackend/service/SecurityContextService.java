package com.hoquangnam.blogbackend.service;

import com.hoquangnam.blogbackend.core.security.model.UserDetailsImpl;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;

import java.util.Optional;
import java.util.UUID;

public interface SecurityContextService {
    SecurityContext getContext();
    boolean isAuthenticated();

    Optional<Authentication> getAuthentication();

    void setAuthentication(Authentication authentication);

    Optional<UserDetailsImpl> getPrincipal(Authentication authentication);
    Optional<UUID> getPrincipalId(Authentication authentication);
}