package com.hoquangnam.blogbackend.service;

import com.hoquangnam.blogbackend.constant.Permission;
import com.hoquangnam.blogbackend.core.exception.AuthException;
import com.hoquangnam.blogbackend.core.exception.BadRequestException;
import com.hoquangnam.blogbackend.core.exception.UnauthorizedException;
import com.hoquangnam.blogbackend.core.security.permission.HaveAuthor;
import com.hoquangnam.blogbackend.model.dto.TokenResponseDTO;
import org.springframework.security.core.Authentication;

public interface AuthService {
    Authentication authenticate(String username, String password) throws AuthException;
    Authentication authenticate(String accessToken) throws AuthException;
    String encodePassword(String rawPassword);
    void logout(String accessToken) throws BadRequestException;
    TokenResponseDTO login(String username, String password) throws AuthException;
    void checkPermission(Authentication authentication, HaveAuthor targetDomainObject, Permission permission) throws UnauthorizedException;
}