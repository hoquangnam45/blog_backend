package com.hoquangnam.blogbackend.service;

import com.hoquangnam.blogbackend.core.exception.ResourceNotFoundException;
import com.hoquangnam.blogbackend.core.exception.UnauthorizedException;
import com.hoquangnam.blogbackend.model.dto.BlogDTO;
import org.springframework.security.core.Authentication;

import java.util.UUID;

public interface BlogService {
    BlogDTO createBlog(Authentication authentication, BlogDTO request);
    BlogDTO getBlog(UUID id) throws ResourceNotFoundException;
    void deleteBlog(Authentication authentication, UUID blogId) throws ResourceNotFoundException, UnauthorizedException;
    BlogDTO updateBlog(Authentication authentication, UUID blogId, BlogDTO request) throws ResourceNotFoundException, UnauthorizedException;
}