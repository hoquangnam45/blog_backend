package com.hoquangnam.blogbackend.service.impl;

import com.hoquangnam.blogbackend.constant.Permission;
import com.hoquangnam.blogbackend.core.event.BlogEvent;
import com.hoquangnam.blogbackend.core.exception.ResourceNotFoundException;
import com.hoquangnam.blogbackend.core.exception.UnauthorizedException;
import com.hoquangnam.blogbackend.core.mapper.BlogMapper;
import com.hoquangnam.blogbackend.core.security.model.UserDetailsImpl;
import com.hoquangnam.blogbackend.model.dto.BlogDTO;
import com.hoquangnam.blogbackend.model.dto.TagDTO;
import com.hoquangnam.blogbackend.model.persistence.TagEntity;
import com.hoquangnam.blogbackend.repository.BlogRepository;
import com.hoquangnam.blogbackend.repository.TagRepository;
import com.hoquangnam.blogbackend.repository.UserRepository;
import com.hoquangnam.blogbackend.service.AuthService;
import com.hoquangnam.blogbackend.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
@Transactional
public class BlogServiceImpl implements BlogService {
    private final TagRepository tagRepository;
    private final UserRepository userRepository;
    private final BlogRepository blogRepository;
    private final BlogMapper blogMapper;
    private final AuthService authService;
    private final ApplicationEventPublisher eventPublisher;

    @Autowired
    public BlogServiceImpl(
            BlogMapper blogMapper,
            BlogRepository blogRepository,
            TagRepository tagRepository,
            UserRepository userRepository,
            AuthService authService,
            ApplicationEventPublisher eventPublisher) {
        this.blogMapper = blogMapper;
        this.blogRepository = blogRepository;
        this.tagRepository = tagRepository;
        this.userRepository = userRepository;
        this.authService = authService;
        this.eventPublisher = eventPublisher;
    }

    @Override
    public BlogDTO createBlog(Authentication authentication, BlogDTO request) {
        var principal = (UserDetailsImpl) authentication.getPrincipal();
        var entity = blogMapper.map(request);
        entity.setId(null);
        entity.setAuthoredBy(userRepository.getReferenceById(principal.getId()));
        entity.setTags(getTags(request));
        blogRepository.save(entity);

        // Send an event will trigger handler to save to outbox table which will trigger debezium connector to sync to kafka using wal
        var blogDocument = blogMapper.mapToDocument(entity);
        eventPublisher.publishEvent(BlogEvent.ofCreate(blogDocument));

        return blogMapper.map(entity);
    }

    @Override
    public BlogDTO getBlog(UUID id) throws ResourceNotFoundException {
        var entity = blogRepository.getBlogEntity(id);
        return blogMapper.map(entity);
    }

    @Override
    public void deleteBlog(Authentication authentication, UUID blogId) throws ResourceNotFoundException, UnauthorizedException {
        var entity = blogRepository.getBlogEntity(blogId);
        authService.checkPermission(authentication, entity, Permission.DELETE);
        entity.softDelete();
        eventPublisher.publishEvent(BlogEvent.ofDelete());
    }

    @Override
    public BlogDTO updateBlog(Authentication authentication, UUID blogId, BlogDTO blog) throws ResourceNotFoundException, UnauthorizedException {
        var entity = blogRepository.getBlogEntity(blogId);
        authService.checkPermission(authentication, entity, Permission.UPDATE);
        entity.setTitle(blog.getTitle());
        entity.setContent(blog.getContent());
        entity.setTags(getTags(blog));

        var blogDocument = blogMapper.mapToDocument(entity);
        eventPublisher.publishEvent(BlogEvent.ofUpdate(blogDocument));

        return blogMapper.map(entity);
    }

    private Set<TagEntity> getTags(BlogDTO blogDTO) {
        return Optional.of(blogDTO)
                .map(BlogDTO::getTags)
                .stream()
                .flatMap(Set::stream)
                .map(TagDTO::getKeyword)
                .map(tagRepository::findAllByKeyword)
                .flatMap(List::stream)
                .collect(Collectors.toSet());
    }
}