package com.hoquangnam.blogbackend.service.impl;

import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.hoquangnam.blogbackend.constant.Permission;
import com.hoquangnam.blogbackend.core.exception.AuthException;
import com.hoquangnam.blogbackend.core.exception.BadRequestException;
import com.hoquangnam.blogbackend.core.exception.UnauthorizedException;
import com.hoquangnam.blogbackend.core.security.claims.Claims;
import com.hoquangnam.blogbackend.core.security.model.UserDetailsImpl;
import com.hoquangnam.blogbackend.core.security.permission.HaveAuthor;
import com.hoquangnam.blogbackend.core.security.permission.SameAuthorPermissionEvaluator;
import com.hoquangnam.blogbackend.core.security.providers.jwt.JwtAuthenticationToken;
import com.hoquangnam.blogbackend.core.security.service.TokenService;
import com.hoquangnam.blogbackend.model.dto.TokenResponseDTO;
import com.hoquangnam.blogbackend.service.AuthService;
import com.hoquangnam.blogbackend.service.SecurityContextService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.UUID;

@Service
public class AuthServiceImpl implements AuthService {
    private final AuthenticationManager authenticationManager;
    private final PasswordEncoder passwordEncoder;
    private final SameAuthorPermissionEvaluator permissionEvaluator;
    private final SecurityContextService securityContextService;
    private final TokenService tokenService;

    public AuthServiceImpl(
            AuthenticationManager authenticationManager,
            PasswordEncoder passwordEncoder,
            SameAuthorPermissionEvaluator permissionEvaluator,
            SecurityContextService securityContextService,
            TokenService tokenService) {
        this.authenticationManager = authenticationManager;
        this.passwordEncoder = passwordEncoder;
        this.permissionEvaluator = permissionEvaluator;
        this.securityContextService = securityContextService;
        this.tokenService = tokenService;
    }

    @Override
    public Authentication authenticate(String username, String password) throws AuthException {
        try {
            return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (AuthenticationException e) {
            throw new AuthException(e);
        }
    }

    @Override
    public Authentication authenticate(String accessToken) throws AuthException {
        try {
            return authenticationManager.authenticate(new JwtAuthenticationToken(accessToken));
        } catch (AuthenticationException e) {
            throw new AuthException(e);
        }
    }

    @Override
    public String encodePassword(String rawPassword) {
        return passwordEncoder.encode(rawPassword);
    }

    @Override
    public void logout(String accessToken) throws BadRequestException {
        if (StringUtils.isBlank(accessToken)) {
            return;
        }

        try {
            var decodedJwt = tokenService.decode(accessToken);
            try {
                tokenService.verifyAccessToken(decodedJwt.getToken());
            } catch (SignatureVerificationException e) {
                throw new BadRequestException("Access token signature is invalid");
            }
            var stateId = tokenService.getStateId(decodedJwt).orElse(null);
            var refreshTokenExpiredAt = decodedJwt.getClaim(Claims.REFRESH_EXPIRED_AT.getValue()).asDate();
            if (tokenService.isBlacklist(stateId)) {
                return;
            }
            tokenService.blacklist(stateId, refreshTokenExpiredAt);
        } catch (JWTDecodeException e) {
            throw new BadRequestException("Access token can not be decoded");
        }
    }

    @Override
    public TokenResponseDTO login(String username, String password) throws AuthException {
        var authentication = authenticate(
                username,
                password);
        securityContextService.setAuthentication(authentication);

        var principal = (UserDetailsImpl) authentication.getPrincipal();
        var email = principal.getEmail();
        var id = principal.getId().toString();
        var state = UUID.randomUUID();
        var tokenPair = tokenService.generateTokenPair(state, username, email, id);
        var refreshToken = tokenPair.getKey();
        var accessToken = tokenPair.getValue();
        return TokenResponseDTO.builder().accessToken(accessToken).refreshToken(refreshToken).build();
    }

    @Override
    public void checkPermission(Authentication authentication, HaveAuthor targetDomainObject, Permission permission) throws UnauthorizedException {
        var authenticationUserId = securityContextService.getPrincipalId(authentication).map(UUID::toString).orElse(null);
        if (!permissionEvaluator.hasPermission(authenticationUserId, targetDomainObject, permission)) {
            throw new UnauthorizedException(MessageFormat.format("User with id {0} do not have permission to perform this operation", authenticationUserId));
        }
    }
}