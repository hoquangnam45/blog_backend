package com.hoquangnam.blogbackend.service.impl;

import com.hoquangnam.blogbackend.core.security.model.UserDetailsImpl;
import com.hoquangnam.blogbackend.service.SecurityContextService;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

import static java.util.function.Predicate.not;

@Service
public class SecurityContextServiceImpl implements SecurityContextService {
    @Override
    public SecurityContext getContext() {
        return SecurityContextHolder.getContext();
    }

    @Override
    public boolean isAuthenticated() {
        return Optional.ofNullable(getContext())
                .map(SecurityContext::getAuthentication)
                .filter(not(AnonymousAuthenticationToken.class::isInstance)) // The authentication object from AnonymousFilter this will set if there's no other filter set authentication
                .map(Authentication::isAuthenticated)
                .orElse(false);
    }

    @Override
    public Optional<Authentication> getAuthentication() {
        return Optional.empty();
    }

    @Override
    public void setAuthentication(Authentication authentication) {
        getContext().setAuthentication(authentication);
    }

    @Override
    public Optional<UserDetailsImpl> getPrincipal(Authentication authentication) {
        return Optional.ofNullable(authentication)
                .map(Authentication::getPrincipal)
                .filter(UserDetailsImpl.class::isInstance)
                .map(UserDetailsImpl.class::cast);
    }

    @Override
    public Optional<UUID> getPrincipalId(Authentication authentication) {
        return getPrincipal(authentication)
                .map(UserDetailsImpl::getId);
    }
}