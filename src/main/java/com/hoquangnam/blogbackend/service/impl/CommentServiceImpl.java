package com.hoquangnam.blogbackend.service.impl;

import com.hoquangnam.blogbackend.constant.Permission;
import com.hoquangnam.blogbackend.core.exception.ResourceNotFoundException;
import com.hoquangnam.blogbackend.core.exception.UnauthorizedException;
import com.hoquangnam.blogbackend.core.mapper.CommentMapper;
import com.hoquangnam.blogbackend.core.security.model.UserDetailsImpl;
import com.hoquangnam.blogbackend.model.dto.CommentDTO;
import com.hoquangnam.blogbackend.repository.BlogRepository;
import com.hoquangnam.blogbackend.repository.CommentRepository;
import com.hoquangnam.blogbackend.repository.UserRepository;
import com.hoquangnam.blogbackend.service.AuthService;
import com.hoquangnam.blogbackend.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Transactional
public class CommentServiceImpl implements CommentService {
    private final CommentMapper commentMapper;
    private final CommentRepository commentRepository;
    private final BlogRepository blogRepository;
    private final UserRepository userRepository;
    private final AuthService authService;

    @Autowired
    public CommentServiceImpl(
            CommentMapper commentMapper,
            CommentRepository commentRepository,
            BlogRepository blogRepository,
            UserRepository userRepository,
            AuthService authService) {
        this.commentMapper = commentMapper;
        this.commentRepository = commentRepository;
        this.blogRepository = blogRepository;
        this.userRepository = userRepository;
        this.authService = authService;
    }
    
    @Override
    public CommentDTO createComment(Authentication authentication, CommentDTO request) throws ResourceNotFoundException {
        var entity = commentMapper.map(request);
        var principal = (UserDetailsImpl) authentication.getPrincipal();
        entity.setId(null);
        entity.setBlog(blogRepository.getBlogEntity(request.getBlogId()));
        entity.setUser(userRepository.getReferenceById(principal.getId()));
        entity.setReplyTo(commentRepository.getReplyTo(request.getId()));
        commentRepository.save(entity);
        return commentMapper.map(entity);
    }

    @Override
    public CommentDTO editComment(Authentication authentication, UUID commentId, CommentDTO commentDTO) throws ResourceNotFoundException, UnauthorizedException {
        var entity = commentRepository.getCommentEntity(commentId);
        authService.checkPermission(authentication, entity, Permission.UPDATE);
        var principal = (UserDetailsImpl) authentication.getPrincipal();
        entity.setId(commentId);
        entity.setUser(userRepository.getReferenceById(principal.getId()));
        entity.setContent(commentDTO.getContent());
        return commentMapper.map(entity);
    }

    @Override
    public void deleteComment(Authentication authentication, UUID commentId) throws ResourceNotFoundException, UnauthorizedException {
        var entity = commentRepository.getCommentEntity(commentId);
        authService.checkPermission(authentication, entity, Permission.UPDATE);
        entity.softDelete();
    }

    @Override
    public Set<CommentDTO> getCommentsOnBlog(UUID blogId) throws ResourceNotFoundException {
        var entity = blogRepository.getBlogEntity(blogId);
        return commentRepository.findAllByBlog_Id(entity.getId())
                .stream()
                .map(commentMapper::map)
                .collect(Collectors.toSet());
    }
}