package com.hoquangnam.blogbackend.service.impl;

import com.hoquangnam.blogbackend.core.exception.ResourceConflictException;
import com.hoquangnam.blogbackend.core.exception.ResourceNotFoundException;
import com.hoquangnam.blogbackend.core.mapper.TagMapper;
import com.hoquangnam.blogbackend.model.dto.TagDTO;
import com.hoquangnam.blogbackend.model.persistence.TagEntity;
import com.hoquangnam.blogbackend.repository.TagRepository;
import com.hoquangnam.blogbackend.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class TagServiceImpl implements TagService {
    private final TagRepository tagRepository;
    private final TagMapper tagMapper;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository, TagMapper tagMapper) {
        this.tagRepository = tagRepository;
        this.tagMapper = tagMapper;
    }

    @Override
    public void deleteTag(String tag) throws ResourceNotFoundException {
        var entity = tagRepository.getTagEntity(tag);
        entity.softDelete();
    }

    @Override
    public TagDTO createTag(String tag) throws ResourceConflictException {
        if (tagRepository.existsByKeyword(tag)) {
            throw new ResourceConflictException("Try to create an already existed tag");
        }
        var entity = TagEntity.builder()
                .keyword(tag)
                .build();
        tagRepository.save(entity);
        return tagMapper.map(entity);
    }

    @Override
    public List<TagDTO> getTrendingTag(Integer topTrendingNumber) {
        var pageRequest = PageRequest.of(0, topTrendingNumber);
        var page = tagRepository.findMostUsedTags(pageRequest);
        return tagMapper.mapEntities(page.toList());
    }

    @Override
    public Page<TagDTO> searchTag(String keyword, PageRequest pageRequest) {
        return tagRepository.findAllByKeywordStartingWith(keyword, pageRequest).map(tagMapper::map);
    }
}