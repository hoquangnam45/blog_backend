package com.hoquangnam.blogbackend.service.impl;

import com.hoquangnam.blogbackend.constant.Role;
import com.hoquangnam.blogbackend.core.exception.ResourceConflictException;
import com.hoquangnam.blogbackend.core.exception.UserNotFoundException;
import com.hoquangnam.blogbackend.core.mapper.UserMapper;
import com.hoquangnam.blogbackend.core.security.model.UserDetailsImpl;
import com.hoquangnam.blogbackend.model.dto.AuthDTO;
import com.hoquangnam.blogbackend.model.dto.UserDTO;
import com.hoquangnam.blogbackend.repository.RoleRepository;
import com.hoquangnam.blogbackend.repository.UserRepository;
import com.hoquangnam.blogbackend.service.AuthService;
import com.hoquangnam.blogbackend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final AuthService authService;
    private final RoleRepository roleRepository;

    @Autowired
    public UserServiceImpl(
            UserMapper userMapper,
            UserRepository userRepository,
            AuthService authService,
            RoleRepository roleRepository) {
        this.userMapper = userMapper;
        this.userRepository = userRepository;
        this.authService = authService;
        this.roleRepository = roleRepository;
    }

    @Override
    public void follow(Authentication authentication, UUID followingId) throws UserNotFoundException {
        var userId = ((UserDetailsImpl) authentication.getPrincipal()).getId();
        var user = userRepository.getReferenceById(userId);
        var followed = userRepository.findById(followingId).orElseThrow(UserNotFoundException::new);
        var followers = followed.getFollowers();
        if (followers.contains(user)) {
            return;
        }
        followers.add(user);
    }

    @Override
    public void unfollow(Authentication authentication, UUID followingId) throws UserNotFoundException {
        var userId = ((UserDetailsImpl) authentication.getPrincipal()).getId();
        var user = userRepository.getReferenceById(userId);
        var followed = userRepository.findById(followingId).orElseThrow(UserNotFoundException::new);
        var followers = followed.getFollowers();
        if (!followers.contains(user)) {
            return;
        }
        followers.remove(user);
    }

    @Override
    public UserDTO createUser(AuthDTO request) throws ResourceConflictException {
        if (userRepository.existsByEmail(request.getEmail())) {
            throw new ResourceConflictException("Error: Email is already taken!");
        }
        if (userRepository.existsByUsername(request.getUsername())) {
            throw new ResourceConflictException("Error: Username is already taken!");
        }
        var entity = userMapper.map(request);
        var roles = Stream.of(Role.BLOGGER).map(Role::getId).map(roleRepository::getReferenceById).collect(Collectors.toCollection(HashSet::new));
        entity.setRoles(roles);
        entity.setPassword(authService.encodePassword(entity.getPassword()));
        return userMapper.map(userRepository.save(entity));
    }
}