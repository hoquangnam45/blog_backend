package com.hoquangnam.blogbackend.service.impl;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch.core.SearchRequest;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import com.hoquangnam.blogbackend.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class SearchServiceImpl implements SearchService {
    private final ElasticsearchClient client;

    @Autowired
    public SearchServiceImpl(ElasticsearchClient client) {
        this.client = client;
    }

    @Override
    public <T> SearchResponse<T> search(SearchRequest searchRequest, Class<T> documentClazz) throws IOException {
        return client.search(searchRequest, documentClazz);
    }
}
