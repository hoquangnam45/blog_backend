package com.hoquangnam.blogbackend.service;

import com.hoquangnam.blogbackend.core.exception.ResourceNotFoundException;
import com.hoquangnam.blogbackend.core.exception.UnauthorizedException;
import com.hoquangnam.blogbackend.model.dto.CommentDTO;
import org.springframework.security.core.Authentication;

import java.util.Set;
import java.util.UUID;

public interface CommentService {
    CommentDTO createComment(Authentication authentication, CommentDTO request) throws ResourceNotFoundException;

    CommentDTO editComment(Authentication authentication, UUID commentId, CommentDTO commentDTO) throws ResourceNotFoundException, UnauthorizedException;

    void deleteComment(Authentication authentication, UUID commentId) throws ResourceNotFoundException, UnauthorizedException;

    Set<CommentDTO> getCommentsOnBlog(UUID blogId) throws ResourceNotFoundException;
}