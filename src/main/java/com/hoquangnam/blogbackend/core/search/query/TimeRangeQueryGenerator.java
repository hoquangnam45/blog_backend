package com.hoquangnam.blogbackend.core.search.search.query;

import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import com.hoquangnam.blogbackend.core.search.search.filter.TimeRangeFilter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.time.OffsetDateTime;
import java.util.Optional;

@Getter
@Builder
@AllArgsConstructor
public class TimeRangeQueryGenerator implements QueryGenerator {
    private TimeRangeFilter filter;

    public Query toQuery(String field) {
        if (filter == null || filter.getFrom() == null && filter.getTo() == null || field == null) {
            return null;
        }
        var from = filter.getFrom();
        var to = filter.getTo();
        var builder = new Query.Builder();
        var fromString = Optional.ofNullable(from).map(OffsetDateTime::toString).orElse(null);
        var toString = Optional.ofNullable(to).map(OffsetDateTime::toString).orElse(null);
        return builder.range(b -> b.field(field).from(fromString).to(toString)).build();
    }
}