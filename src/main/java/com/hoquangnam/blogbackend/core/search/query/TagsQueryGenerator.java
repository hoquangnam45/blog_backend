package com.hoquangnam.blogbackend.core.search.search.query;

import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import com.hoquangnam.blogbackend.core.search.search.filter.TagsFilter;
import com.hoquangnam.blogbackend.core.search.search.filter.TextFilter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Getter
@Builder
@AllArgsConstructor
public class TagsQueryGenerator implements QueryGenerator {
    private TagsFilter filter;

    public Query toQuery(String field) {
        if (filter == null || filter.getTags() == null || field == null) {
            return null;
        }
        var tags = filter.getTags();
        var queryList = Optional.ofNullable(tags)
                .stream()
                .flatMap(List::stream)
                .map(TextFilter.builder()::term)
                .map(item -> item.caseSensitive(true)
                        .filterType(TextFilter.Type.EXACT)
                        .build())
                .map(TextQueryGenerator.builder()::filter)
                .map(TextQueryGenerator.TextQueryGeneratorBuilder::build)
                .map(item -> item.toQuery(field))
                .collect(Collectors.toList());

        var builder = new Query.Builder();
        return builder.bool(b -> b.must(queryList)).build();
    }
}