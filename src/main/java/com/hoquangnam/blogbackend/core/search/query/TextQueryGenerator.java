package com.hoquangnam.blogbackend.core.search.search.query;

import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import com.hoquangnam.blogbackend.core.search.search.filter.TextFilter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.text.MessageFormat;

@Getter
@Builder
@AllArgsConstructor
public class TextQueryGenerator implements QueryGenerator {
    private TextFilter filter;

    public Query toQuery(String field) {
        if (filter == null || filter.getTerm() == null || field == null) {
            return null;
        }
        var filterType = filter.getFilterType();
        var term = filter.getTerm();
        var caseSensitive = filter.isCaseSensitive();
        var builder = new Query.Builder();
        switch (filterType) {
            case EXACT:
                return builder.term(b -> b.field(field)
                        .caseInsensitive(!caseSensitive)
                        .value(term)).build();
            case START_WITH:
                return builder.prefix(b -> b.field(field)
                        .caseInsensitive(!caseSensitive)
                        .value(term)).build();
            case END_WITH:
                return builder.regexp(b -> b.field(field)
                        .caseInsensitive(!caseSensitive)
                        .value(MessageFormat.format("*{0}", term))).build();
            case CONTAIN:
                return builder.wildcard(b -> b.field(field)
                        .caseInsensitive(!caseSensitive)
                        .value(term)).build();
            case REGEX:
                return builder.regexp(b -> b.field(field)
                        .caseInsensitive(!caseSensitive)
                        .value(term)).build();
            default:
                throw new UnsupportedOperationException(MessageFormat.format("Query does not support this filterType {0} yet", filterType.name()));
        }
    }
}