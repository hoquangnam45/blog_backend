package com.hoquangnam.blogbackend.core.search.search.filter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.time.OffsetDateTime;

@Builder
@Getter
@AllArgsConstructor
public class TimeRangeFilter {
    private OffsetDateTime from;
    private OffsetDateTime to;
}