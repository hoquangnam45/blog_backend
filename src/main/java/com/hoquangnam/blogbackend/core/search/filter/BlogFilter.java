package com.hoquangnam.blogbackend.core.search.search.filter;

import co.elastic.clients.elasticsearch._types.SortOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.time.OffsetDateTime;

@Getter
@AllArgsConstructor
@Builder
public class BlogFilter {
    private String tag;
    private String title;
    private String content;
    private String author;
    private Integer from;
    private Integer size;
    private OffsetDateTime createdDateFrom;
    private OffsetDateTime createdDateTo;
    private Integer rating;
    private SortOrder sortOrder;
    private String sortField;
    private String highlightField;
}