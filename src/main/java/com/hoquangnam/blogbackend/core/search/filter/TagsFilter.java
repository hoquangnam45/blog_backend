package com.hoquangnam.blogbackend.core.search.search.filter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
@Builder
public class TagsFilter {
    private List<String> tags;
}