package com.hoquangnam.blogbackend.core.search.search.suggester;

import co.elastic.clients.elasticsearch.core.search.Suggester;

public interface SuggesterGenerator {
    Suggester toSuggester();
}