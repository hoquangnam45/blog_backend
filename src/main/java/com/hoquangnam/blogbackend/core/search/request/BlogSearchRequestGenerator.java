package com.hoquangnam.blogbackend.core.search.search.request;

import co.elastic.clients.elasticsearch._types.FieldSort;
import co.elastic.clients.elasticsearch._types.SortOptions;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch.core.SearchRequest;
import co.elastic.clients.elasticsearch.core.search.Highlight;
import com.hoquangnam.blogbackend.core.search.search.filter.BlogFilter;
import com.hoquangnam.blogbackend.core.search.search.filter.RatingFilter;
import com.hoquangnam.blogbackend.core.search.search.filter.TagsFilter;
import com.hoquangnam.blogbackend.core.search.search.filter.TextFilter;
import com.hoquangnam.blogbackend.core.search.search.filter.TimeRangeFilter;
import com.hoquangnam.blogbackend.core.search.search.query.QueryGenerator;
import com.hoquangnam.blogbackend.core.search.search.query.RatingQueryGenerator;
import com.hoquangnam.blogbackend.core.search.search.query.TagsQueryGenerator;
import com.hoquangnam.blogbackend.core.search.search.query.TextQueryGenerator;
import com.hoquangnam.blogbackend.core.search.search.query.TimeRangeQueryGenerator;
import lombok.experimental.UtilityClass;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@UtilityClass
public class BlogSearchRequestGenerator {
    public static final String AUTHOR_FIELD = "author";
    public static final String TITLE_FIELD = "title";
    public static final String CONTENT_FIELD = "content";
    private static final String CREATED_DATE_FIELD = "createdDate";
    private static final String TAGS_FIELD = "tags";
    private static final String RATING_FIELD = "rating";
    private static final String DELETED_DATE_FIELD = "deletedDate";
    private static final String INDEX_NAME = "blog_index";

    public SearchRequest generateSearchRequest(BlogFilter blogFilter) {
        if (blogFilter == null) {
            return null;
        }

        var authorFilter = TextFilter.builder().term(blogFilter.getAuthor()).build();
        var titleFilter = TextFilter.builder().term(blogFilter.getTitle()).build();
        var contentFilter = TextFilter.builder().term(blogFilter.getContent()).build();
        var tagsFilter = Optional.of(blogFilter).map(BlogFilter::getTag).map(List::of).map(TagsFilter.builder()::tags).orElse(TagsFilter.builder()).build();
        var timeRangeFilter = TimeRangeFilter.builder().to(blogFilter.getCreatedDateTo()).from(blogFilter.getCreatedDateFrom()).build();
        var ratingFilter = RatingFilter.builder().rating(blogFilter.getRating()).build();

        var authorQueryGenerator = TextQueryGenerator.builder().filter(authorFilter).build();
        var titleQueryGenerator = TextQueryGenerator.builder().filter(titleFilter).build();
        var contentQueryGenerator = TextQueryGenerator.builder().filter(contentFilter).build();
        var tagsQueryGenerator = TagsQueryGenerator.builder().filter(tagsFilter).build();
        var timeRangeQueryGenerator = TimeRangeQueryGenerator.builder().filter(timeRangeFilter).build();
        var ratingQueryGenerator = RatingQueryGenerator.builder().filter(ratingFilter).build();

        var builder = new SearchRequest.Builder();

        var queryMap = new HashMap<String, QueryGenerator>() {{
            put(AUTHOR_FIELD, authorQueryGenerator);
            put(TITLE_FIELD, titleQueryGenerator);
            put(CONTENT_FIELD, contentQueryGenerator);
            put(TAGS_FIELD, tagsQueryGenerator);
            put(CREATED_DATE_FIELD, timeRangeQueryGenerator);
            put(RATING_FIELD, ratingQueryGenerator);
        }}.entrySet().stream()
        .map(item -> {
            var key = item.getKey();
            var val = item.getValue().toQuery(key);
            var entry = new AbstractMap.SimpleEntry<>(key, val);
            return (Map.Entry<String, Query>) entry;
        })
        .filter(item -> item.getValue() != null)
        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        var queryList = new ArrayList<>(queryMap.values());

        if (queryMap.containsKey(blogFilter.getHighlightField())) {
            var b = new Highlight.Builder();
            b.highlightQuery(queryMap.get(blogFilter.getHighlightField()));
            var highlight = b.build();
            builder.highlight(highlight);
        }

        if (blogFilter.getSortOrder() != null && blogFilter.getSortField() != null) {
            var fb = new FieldSort.Builder();
            fb.field(blogFilter.getSortField())
                    .order(blogFilter.getSortOrder());
            var sb = new SortOptions.Builder();
            sb.field(fb.build());
            builder.sort(sb.build());
        }

        var query = new Query.Builder()
                .bool(b -> {
                    b.must(queryList);
                    var haveDeleted = new Query.Builder().exists(el -> el.field(DELETED_DATE_FIELD)).build();
                    b.mustNot(haveDeleted);
                    return b;
                }).build();
        var from = blogFilter.getFrom();
        var size = blogFilter.getSize();

        return builder
                .index(INDEX_NAME)
                .query(query)
                .from(from)
                .size(size)
                .build();
    }
}