package com.hoquangnam.blogbackend.core.search.search.query;

import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import com.hoquangnam.blogbackend.core.search.search.filter.RatingFilter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@AllArgsConstructor
public class RatingQueryGenerator implements QueryGenerator {
    private RatingFilter filter;
    public Query toQuery(String field) {
        if (filter == null || filter.getRating() == null || field == null) {
            return null;
        }
        var rating = filter.getRating();
        var builder = new Query.Builder();
        var ratingString = rating.toString();
        return builder.range(b -> b
                    .field(field)
                    .from(ratingString))
                    .build();
    }
}