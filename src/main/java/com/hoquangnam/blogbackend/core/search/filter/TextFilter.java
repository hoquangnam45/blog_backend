package com.hoquangnam.blogbackend.core.search.search.filter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class TextFilter {
    @Builder.Default
    protected Type filterType = Type.CONTAIN;
    @Builder.Default
    protected boolean caseSensitive = false;
    protected String term;

    public enum Type {
        EXACT,
        START_WITH,
        END_WITH,
        CONTAIN,
        REGEX
    }
}