package com.hoquangnam.blogbackend.core.search.search.sort;

import co.elastic.clients.elasticsearch._types.SortOptions;

public interface SortGenerator {
    SortOptions toSort();
}