package com.hoquangnam.blogbackend.core.search.document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.OffsetDateTime;
import java.util.Set;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@SuperBuilder
public class BlogDocument {
    private UUID id;
    private UserDocument authoredBy;
    private String title;
    private String content;
    private OffsetDateTime createdDate;
    private OffsetDateTime updatedDate;
    private Set<TagDocument> tags;

    @Getter
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class UserDocument {
        private UUID id;
        private String username;
        private String email;
    }

    @Getter
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class TagDocument {
        private UUID id;
        private String keyword;
    }
}