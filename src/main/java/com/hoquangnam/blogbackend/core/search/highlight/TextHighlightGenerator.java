package com.hoquangnam.blogbackend.core.search.search.highlight;

import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch.core.search.Highlight;
import lombok.experimental.UtilityClass;

@UtilityClass
public class TextHighlightGenerator {
    Highlight toHighlight(Query query) {
        return new Highlight.Builder().highlightQuery(query).build();
    }
}