package com.hoquangnam.blogbackend.core.search.search.filter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@AllArgsConstructor
@Builder
public class RatingFilter {
    private Integer rating;
}