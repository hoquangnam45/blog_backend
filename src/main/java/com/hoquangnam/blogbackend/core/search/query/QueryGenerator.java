package com.hoquangnam.blogbackend.core.search.search.query;

import co.elastic.clients.elasticsearch._types.query_dsl.Query;

public interface QueryGenerator {
    Query toQuery(String field);
}