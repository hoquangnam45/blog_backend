package com.hoquangnam.blogbackend.core.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class BadRequestException extends Exception {
    private static final long serialVersionUID = 6337347770594807394L;

    public BadRequestException(String message) {
        super(message);
    }
}