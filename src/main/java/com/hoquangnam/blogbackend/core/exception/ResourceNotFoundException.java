package com.hoquangnam.blogbackend.core.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ResourceNotFoundException extends Exception {
    private static final long serialVersionUID = 2981916950415735424L;

    public ResourceNotFoundException(String message) {
        super(message);
    }
}