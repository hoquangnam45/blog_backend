package com.hoquangnam.blogbackend.core.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class UserNotFoundException extends Exception{
    private static final long serialVersionUID = 7888004163318025050L;

    public UserNotFoundException(String message) {
        super(message);
    }
}