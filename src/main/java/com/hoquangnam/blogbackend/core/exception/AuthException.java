package com.hoquangnam.blogbackend.core.exception;

import org.springframework.security.core.AuthenticationException;

public class AuthException extends Exception {
    public AuthException(AuthenticationException e) {
        super(e.getLocalizedMessage(), e.getCause());
    }

    public AuthException(String msg) {
        super(msg);
    }
}