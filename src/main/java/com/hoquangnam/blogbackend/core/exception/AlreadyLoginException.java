package com.hoquangnam.blogbackend.core.exception;

public class AlreadyLoginException extends Exception {
    public AlreadyLoginException(String msg) {
        super(msg);
    }
}