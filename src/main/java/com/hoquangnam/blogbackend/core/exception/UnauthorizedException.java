package com.hoquangnam.blogbackend.core.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class UnauthorizedException extends Exception {
    private static final long serialVersionUID = -3559101692540055278L;

    public UnauthorizedException(String message) {
        super(message);
    }
}