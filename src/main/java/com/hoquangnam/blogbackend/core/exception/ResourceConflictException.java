package com.hoquangnam.blogbackend.core.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ResourceConflictException extends Exception {
    public ResourceConflictException(String message) {
        super(message);
    }
}