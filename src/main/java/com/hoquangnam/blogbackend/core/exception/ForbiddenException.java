package com.hoquangnam.blogbackend.core.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ForbiddenException extends Exception {
    private static final long serialVersionUID = -8455801596084638515L;

    public ForbiddenException(String message) {
        super(message);
    }
}