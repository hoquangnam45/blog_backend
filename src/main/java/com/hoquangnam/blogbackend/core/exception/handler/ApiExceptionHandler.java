package com.hoquangnam.blogbackend.core.exception.handler;

import com.hoquangnam.blogbackend.core.exception.AlreadyLoginException;
import com.hoquangnam.blogbackend.core.exception.AuthException;
import com.hoquangnam.blogbackend.core.exception.BadRequestException;
import com.hoquangnam.blogbackend.core.exception.ForbiddenException;
import com.hoquangnam.blogbackend.core.exception.ResourceConflictException;
import com.hoquangnam.blogbackend.core.exception.ResourceNotFoundException;
import com.hoquangnam.blogbackend.core.exception.UnauthorizedException;
import com.hoquangnam.blogbackend.core.exception.UserNotFoundException;
import com.hoquangnam.blogbackend.model.dto.ErrorMessage;
import com.hoquangnam.blogbackend.model.dto.Response;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import reactor.util.Logger;
import reactor.util.Loggers;

import java.io.IOException;
import java.text.MessageFormat;

@RestControllerAdvice
public class ApiExceptionHandler {
    private final Logger logger = Loggers.getLogger(ApiExceptionHandler.class);

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Response<ErrorMessage> somethingHadHappened(Exception e) {
        logger.error(MessageFormat.format("Exception happened without handling: {0}", e.getCause().getCause()));
        return Response.<ErrorMessage>builder()
                .statusCode(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .success(false)
                .payload(new ErrorMessage(e.getLocalizedMessage()))
                .build();
    }

    @ExceptionHandler(AlreadyLoginException.class)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Response<ErrorMessage> alreadyLoginException(AlreadyLoginException e) {
        return Response.<ErrorMessage>builder()
                .statusCode(HttpStatus.NO_CONTENT.value())
                .success(false)
                .payload(new ErrorMessage(e.getLocalizedMessage()))
                .build();
    }

    @ExceptionHandler(BadCredentialsException.class)
    @ResponseStatus(HttpStatus.OK)
    public Response<ErrorMessage> badCredentialException(BadCredentialsException e) {
        return Response.<ErrorMessage>builder()
                .statusCode(HttpStatus.UNAUTHORIZED.value())
                .success(false)
                .payload(new ErrorMessage(e.getLocalizedMessage()))
                .build();
    }

    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Response<ErrorMessage> badRequestException(BadRequestException e) {
        return Response.<ErrorMessage>builder()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .success(false)
                .payload(new ErrorMessage(e.getLocalizedMessage()))
                .build();
    }

    @ExceptionHandler(ForbiddenException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public Response<ErrorMessage> forbiddenException(ForbiddenException e) {
        return Response.<ErrorMessage>builder()
                .statusCode(HttpStatus.FORBIDDEN.value())
                .success(false)
                .payload(new ErrorMessage(e.getLocalizedMessage()))
                .build();
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Response<ErrorMessage> notFoundException(ResourceNotFoundException e) {
        return Response.<ErrorMessage>builder()
                .statusCode(HttpStatus.NOT_FOUND.value())
                .success(false)
                .payload(new ErrorMessage(e.getLocalizedMessage()))
                .build();
    }

    @ExceptionHandler(UnauthorizedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public Response<ErrorMessage> unauthorizedException(UnauthorizedException e) {
        return Response.<ErrorMessage>builder()
                .statusCode(HttpStatus.UNAUTHORIZED.value())
                .success(false)
                .payload(new ErrorMessage(e.getLocalizedMessage()))
                .build();
    }

    @ExceptionHandler(UserNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Response<ErrorMessage> userNotFoundException(UnauthorizedException e) {
        return Response.<ErrorMessage>builder()
                .statusCode(HttpStatus.NOT_FOUND.value())
                .success(false)
                .payload(new ErrorMessage(e.getLocalizedMessage()))
                .build();
    }

    @ExceptionHandler(ResourceConflictException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public Response<ErrorMessage> resourceConflictException(ResourceConflictException e) {
        return Response.<ErrorMessage>builder()
                .statusCode(HttpStatus.CONFLICT.value())
                .success(false)
                .payload(new ErrorMessage(e.getLocalizedMessage()))
                .build();
    }

    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public Response<ErrorMessage> accessDeniedException(AccessDeniedException e) {
        return Response.<ErrorMessage>builder()
                .statusCode(HttpStatus.FORBIDDEN.value())
                .success(false)
                .payload(new ErrorMessage(e.getLocalizedMessage()))
                .build();
    }
    
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Response<ErrorMessage> methodArgumentNotValidException(MethodArgumentNotValidException e) {
        return Response.<ErrorMessage>builder()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .success(false)
                .payload(new ErrorMessage(e.getLocalizedMessage()))
                .build();
    }

    @ExceptionHandler(AuthException.class)
    @ResponseStatus(HttpStatus.OK)
    public Response<ErrorMessage> authException(AuthException e){
        return Response.<ErrorMessage>builder()
                .statusCode(HttpStatus.UNAUTHORIZED.value())
                .success(false)
                .payload(new ErrorMessage(e.getLocalizedMessage()))
                .build();
    }

    @ExceptionHandler(IOException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Response<ErrorMessage> ioMessage(IOException e) {
        logger.error(MessageFormat.format("IO Exception: {0}", e.getCause()));
        return Response.<ErrorMessage>builder()
                .statusCode(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .success(false)
                .payload(new ErrorMessage(e.getLocalizedMessage()))
                .build();
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Response<ErrorMessage> methodNotSupported(HttpRequestMethodNotSupportedException e) {
        return Response.<ErrorMessage>builder()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .success(false)
                .payload(new ErrorMessage(e.getLocalizedMessage()))
                .build();
    }
}