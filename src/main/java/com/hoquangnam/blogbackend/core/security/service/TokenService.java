package com.hoquangnam.blogbackend.core.security.service;

import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.hoquangnam.blogbackend.core.exception.BadRequestException;
import com.hoquangnam.blogbackend.model.dto.TokenRequestDTO;
import com.hoquangnam.blogbackend.model.dto.TokenResponseDTO;
import org.apache.commons.lang3.tuple.Pair;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

public interface TokenService {
    Pair<String, String> generateTokenPair(UUID state, String username, String email, String userId);

    DecodedJWT verifyAccessToken(String accessToken) throws JWTVerificationException;

    DecodedJWT verifyRefreshToken(String refreshToken) throws JWTVerificationException;

    void blacklist(String stateId, Date refreshTokenExpiredAt);

    boolean isBlacklist(String stateId);

    DecodedJWT decode(String token) throws JWTDecodeException;

    Optional<String> getStateId(DecodedJWT token);

    Optional<String> getAccessToken(HttpServletRequest request);

    TokenResponseDTO refreshToken(TokenRequestDTO request) throws BadRequestException;
}