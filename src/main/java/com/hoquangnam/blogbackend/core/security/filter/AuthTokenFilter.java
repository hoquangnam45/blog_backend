package com.hoquangnam.blogbackend.core.security.filter;

import com.hoquangnam.blogbackend.core.exception.AuthException;
import com.hoquangnam.blogbackend.core.security.service.TokenService;
import com.hoquangnam.blogbackend.service.AuthService;
import com.hoquangnam.blogbackend.service.SecurityContextService;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * This should not be a spring bean, since it will auto register by spring security bypassing the config
 * in {@link WebSecurity} and {@link HttpSecurity}
 */
public class AuthTokenFilter implements Filter {
    private final AuthService authService;
    private final SecurityContextService securityContextService;
    private final TokenService tokenService;

    public AuthTokenFilter(AuthService authService, SecurityContextService securityContextService, TokenService tokenService) {
        this.authService = authService;
        this.securityContextService = securityContextService;
        this.tokenService = tokenService;
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        if (securityContextService.isAuthenticated()) {
            chain.doFilter(servletRequest, servletResponse);
            return;
        }
        try {
            var accessToken = tokenService.getAccessToken((HttpServletRequest) servletRequest).orElse(null);
            if (accessToken == null) {
                chain.doFilter(servletRequest, servletResponse);
                return;
            }
            var authentication = authService.authenticate(accessToken);
            securityContextService.setAuthentication(authentication);
            chain.doFilter(servletRequest, servletResponse);
        } catch (AuthException e) {
            throw new InsufficientAuthenticationException(e.getLocalizedMessage());
        }
    }
}