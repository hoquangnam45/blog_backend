package com.hoquangnam.blogbackend.core.security.service.impl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.Payload;
import com.hoquangnam.blogbackend.core.exception.BadRequestException;
import com.hoquangnam.blogbackend.core.security.claims.Claims;
import com.hoquangnam.blogbackend.core.security.service.TokenService;
import com.hoquangnam.blogbackend.model.dto.TokenRequestDTO;
import com.hoquangnam.blogbackend.model.dto.TokenResponseDTO;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.text.MessageFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Service
public class TokenServiceImpl implements TokenService {
    private static final String AUTHORIZATION_HEADER = "Authorization";
    private final RedisTemplate<Object, Object> redisClient;
    private final Duration accessTokenExpireDuration;
    private final Duration refreshTokenExpireDuration;
    private final Algorithm accessTokenSignMethod;
    private final Algorithm accessTokenVerifyMethod;
    private final Algorithm refreshTokenSignMethod;
    private final Algorithm refreshTokenVerifyMethod;
    private final String issuer;

    @Autowired
    public TokenServiceImpl(
            RedisTemplate<Object, Object> redisClient,
            @Value("${security.jwt.accessToken.expirationInMin}") int accessTokenExpireDuration,
            @Value("${security.jwt.refreshToken.expirationInMin}") int refreshTokenExpireDuration,
            @Value("${security.jwt.issuer}") String issuer) throws NoSuchAlgorithmException {
        this.redisClient = redisClient;
        this.accessTokenExpireDuration = Duration.ofMinutes(accessTokenExpireDuration);
        this.refreshTokenExpireDuration = Duration.ofMinutes(refreshTokenExpireDuration);
        var keyPairGenerator = KeyPairGenerator.getInstance("EC");
        var accessTokenKeyPair = keyPairGenerator.generateKeyPair();
        this.accessTokenSignMethod = Algorithm.ECDSA512(null, (ECPrivateKey) accessTokenKeyPair.getPrivate());
        this.accessTokenVerifyMethod = Algorithm.ECDSA512((ECPublicKey) accessTokenKeyPair.getPublic(), null);
        var refreshTokenKeyPair = keyPairGenerator.generateKeyPair();
        this.refreshTokenSignMethod = Algorithm.ECDSA512(null, (ECPrivateKey) refreshTokenKeyPair.getPrivate());
        this.refreshTokenVerifyMethod = Algorithm.ECDSA512((ECPublicKey) refreshTokenKeyPair.getPublic(), null);
        this.issuer = issuer;
    }

    @Override
    public void blacklist(String stateId, Date refreshTokenExpiredAt) {
        Duration expiredDuration = Duration.between(Instant.now(), refreshTokenExpiredAt.toInstant());
        redisClient.opsForValue().set(stateId, true, expiredDuration);
    }

    /**
     * If token is null then it's considered blacklist, or else return redis cache check
     */
    @Override
    public boolean isBlacklist(String stateId) {
        return Optional.ofNullable(stateId)
                .map(redisClient::hasKey)
                .orElse(true);
    }

    @Override
    public Pair<String, String> generateTokenPair(UUID state, String username, String email, String userId) {
        var now = Instant.now();
        var issuedAt = Date.from(now);
        var accessTokenExpiredAt = Date.from(now.plus(accessTokenExpireDuration));
        var refreshTokenExpiredAt = Date.from(now.plus(refreshTokenExpireDuration));
        var accessTokenClaimMap = new HashMap<String, Object>() {{
            put(Claims.EMAIL.getValue(), email);
            put(Claims.ID.getValue(), userId);
            put(Claims.USERNAME.getValue(), username);
            put(Claims.STATE.getValue(), state.toString());
            put(Claims.REFRESH_EXPIRED_AT.getValue(), refreshTokenExpiredAt);
        }};
        var refreshTokenClaimMap = new HashMap<String, Object>() {{
            put(Claims.STATE.getValue(), state.toString());
        }};

        var accessToken = generateToken(accessTokenClaimMap, userId, accessTokenSignMethod, issuedAt, accessTokenExpiredAt);
        var refreshToken = generateToken(refreshTokenClaimMap, userId, refreshTokenSignMethod, issuedAt, refreshTokenExpiredAt);
        return Pair.of(refreshToken, accessToken);
    }

    @Override
    public DecodedJWT verifyAccessToken(String accessToken) throws JWTVerificationException {
        return verify(accessToken, accessTokenVerifyMethod);
    }

    @Override
    public DecodedJWT verifyRefreshToken(String refreshToken) throws JWTVerificationException {
        return verify(refreshToken, refreshTokenVerifyMethod);
    }

    @Override
    public DecodedJWT decode(String token) throws JWTDecodeException {
        return JWT.decode(token);
    }

    @Override
    public Optional<String> getStateId(DecodedJWT token) {
        return Optional.ofNullable(token)
                .map(Payload::getClaims)
                .map(it -> it.get(Claims.STATE.getValue()))
                .map(Claim::asString)
                .map("token-"::concat);
    }

    @Override
    public Optional<String> getAccessToken(HttpServletRequest request) {
        return Optional.ofNullable(request)
                .map(it -> it.getHeader(AUTHORIZATION_HEADER))
                .map(it -> it.split("\\s+"))
                .filter(it -> it.length == 2 && it[0].equalsIgnoreCase("bearer"))
                .map(it -> it[1]);
    }

    /**
     * Refresh token rotation:
     * Reference: <a href="https://auth0.com/docs/secure/tokens/refresh-tokens/refresh-token-rotation"/>
     */
    @Override
    public TokenResponseDTO refreshToken(TokenRequestDTO request) throws BadRequestException {
        var refreshToken = request.getRefreshToken();
        if (StringUtils.isBlank(refreshToken)) {
            throw new BadRequestException("Can't refresh without missing refresh token");
        }
        try {
            var decodedJWT = verifyRefreshToken(refreshToken);
            var stateId = getStateId(decodedJWT).orElse(null);
            if (isBlacklist(stateId)) {
                throw new BadRequestException(MessageFormat.format("Token {0} had already been blocked from refreshing", refreshToken));
            }
            var refreshTokenExpiredAt = decodedJWT.getExpiresAt();
            blacklist(stateId, refreshTokenExpiredAt);
            var claims = decodedJWT.getClaims();
            var username = claims.get(Claims.USERNAME.getValue()).asString();
            var id = claims.get(Claims.ID.getValue()).asString();
            var email = claims.get(Claims.EMAIL.getValue()).asString();
            var state = UUID.randomUUID();
            var tokenPair = generateTokenPair(state, username, email, id);
            var newRefreshToken = tokenPair.getKey();
            var newAccessToken = tokenPair.getValue();
            return TokenResponseDTO.builder().accessToken(newAccessToken).refreshToken(newRefreshToken).build();
        }
        catch (JWTVerificationException e) {
            throw new BadRequestException("Refresh token is not valid");
        }
    }

    private String generateToken(Map<String, ?> claimMap, String subject, Algorithm signMethod, Date issuedAt, Date expiredAt) {
        return JWT.create()
                .withPayload(claimMap)
                .withIssuer(issuer)
                .withIssuedAt(issuedAt)
                .withJWTId(UUID.randomUUID().toString())
                .withExpiresAt(expiredAt)
                .withSubject(subject)
                .sign(signMethod);
    }

    private DecodedJWT verify(String authToken, Algorithm verifyMethod) throws JWTVerificationException {
        return JWT.require(verifyMethod).withIssuer(issuer).build().verify(authToken);
    }
}