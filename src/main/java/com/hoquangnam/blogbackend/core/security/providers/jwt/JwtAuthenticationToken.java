package com.hoquangnam.blogbackend.core.security.providers.jwt;

import com.hoquangnam.blogbackend.core.security.model.UserDetailsImpl;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

@Getter
public class JwtAuthenticationToken implements Authentication {
    private final String token;
    private final String username;
    private final UserDetails principal;
    private final Collection<? extends GrantedAuthority> authorities;
    private final UUID id;
    private boolean authenticated;

    public JwtAuthenticationToken(String token) {
        this.authenticated = false;
        this.principal = null;
        this.token = token;
        this.id = null;
        this.authorities = new ArrayList<>();
        this.username = StringUtils.EMPTY;
    }

    public JwtAuthenticationToken(UserDetailsImpl principal, String token) {
        this.authenticated = true;
        this.principal = principal;
        this.token = token;
        this.id = principal.getId();
        this.authorities = principal.getAuthorities();
        this.username = principal.getUsername();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public Object getCredentials() {
        return token;
    }

    @Override
    public Object getDetails() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return principal;
    }

    @Override
    public boolean isAuthenticated() {
        return authenticated;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
        authenticated = isAuthenticated;
    }

    @Override
    public String getName() {
        return username;
    }
}