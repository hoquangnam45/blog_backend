package com.hoquangnam.blogbackend.core.security.permission;

import com.hoquangnam.blogbackend.constant.Permission;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import java.util.Optional;

@Component
public class SameAuthorPermissionEvaluator {
    public boolean hasPermission(@Nullable String authenticationUserId, Object targetDomainObject, Permission permission) {
        if (authenticationUserId == null) {
            return false;
        }
        return Optional.ofNullable(targetDomainObject)
                .filter(it -> HaveAuthor.class.isAssignableFrom(it.getClass()))
                .map(HaveAuthor.class::cast)
                .map(HaveAuthor::getAuthorId)
                .map(authenticationUserId::equals)
                .orElse(false);
    }
}