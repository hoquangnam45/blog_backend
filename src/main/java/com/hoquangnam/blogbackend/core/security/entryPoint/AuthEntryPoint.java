package com.hoquangnam.blogbackend.core.security.entryPoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hoquangnam.blogbackend.core.exception.AuthException;
import com.hoquangnam.blogbackend.core.exception.handler.ApiExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AuthEntryPoint implements AuthenticationEntryPoint {
    private final ApiExceptionHandler exceptionHandler;

    @Autowired
    public AuthEntryPoint(ApiExceptionHandler exceptionHandler) {
        this.exceptionHandler = exceptionHandler;
    }

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException)
            throws IOException {
        var message = exceptionHandler.authException(new AuthException(authException));
        ObjectMapper mapper = new ObjectMapper();
        String msg =  mapper.writeValueAsString(message);
        response.setContentType(MediaType.APPLICATION_JSON.toString());
        response.getWriter().print(msg);
    }
}