package com.hoquangnam.blogbackend.core.security.claims;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Claims {
    EMAIL("email"),
    ID("id"),
    USERNAME("username"),
    STATE("state"),
    REFRESH_EXPIRED_AT("refreshExpiredAt");

    private final String value;
}