package com.hoquangnam.blogbackend.core.security.permission;

public interface HaveAuthor {
    String getAuthorId();
}