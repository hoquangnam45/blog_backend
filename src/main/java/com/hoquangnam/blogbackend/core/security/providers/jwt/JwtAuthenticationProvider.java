package com.hoquangnam.blogbackend.core.security.providers.jwt;

import com.auth0.jwt.exceptions.JWTVerificationException;
import com.hoquangnam.blogbackend.core.security.claims.Claims;
import com.hoquangnam.blogbackend.core.security.model.UserDetailsImpl;
import com.hoquangnam.blogbackend.core.security.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.UUID;

@Component
public class JwtAuthenticationProvider implements AuthenticationProvider {
    private final TokenService tokenService;

    @Autowired
    public JwtAuthenticationProvider(TokenService tokenService) {
        this.tokenService = tokenService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        var token = ((JwtAuthenticationToken) authentication).getToken();
        try {
            var decodedJwt = tokenService.verifyAccessToken(token);
            var stateId = tokenService.getStateId(decodedJwt).orElse(null);
            if (tokenService.isBlacklist(stateId)) {
                throw new InsufficientAuthenticationException("Access token had been blacklisted");
            }
            var claims = decodedJwt.getClaims();
            var username = claims.get(Claims.USERNAME.getValue()).asString();
            var id = UUID.fromString(claims.get(Claims.ID.getValue()).asString());
            var email = claims.get(Claims.EMAIL.getValue()).asString();
            UserDetailsImpl userDetails = UserDetailsImpl.builder()
                    .username(username)
                    .id(id)
                    .email(email)
                    .password(null)
                    .authorities(new ArrayList<>())
                    .build();
            return new JwtAuthenticationToken(userDetails, token);
        }
        catch (JWTVerificationException e) {
            throw new InsufficientAuthenticationException("Can't verify access token");
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication == JwtAuthenticationToken.class;
    }
}