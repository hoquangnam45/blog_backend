package com.hoquangnam.blogbackend.core.mapper;

import com.hoquangnam.blogbackend.model.dto.CommentDTO;
import com.hoquangnam.blogbackend.model.persistence.CommentEntity;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(
        componentModel = "spring",
        uses = { UserMapper.class },
        injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
public interface CommentMapper {
    @Mapping(target = "createdDate", ignore = true)
    @Mapping(target = "updatedDate", ignore = true)
    @Mapping(target = "lastModifiedDate", ignore = true)
    @Mapping(target = "deletedDate", ignore = true)
    @Mapping(target = "blog", ignore = true)
    @Mapping(target = "replyTo", ignore = true)
    @Mapping(target = "user", ignore = true)
    CommentEntity map(CommentDTO CommentDTO);

    @Mapping(target = "blogId", source = "blog.id")
    @Mapping(target = "replyId", source = "replyTo.id")
    CommentDTO map(CommentEntity commentEntity);
}