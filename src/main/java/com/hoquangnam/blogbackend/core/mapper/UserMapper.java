package com.hoquangnam.blogbackend.core.mapper;

import com.hoquangnam.blogbackend.core.search.document.BlogDocument;
import com.hoquangnam.blogbackend.core.security.model.UserDetailsImpl;
import com.hoquangnam.blogbackend.model.dto.AuthDTO;
import com.hoquangnam.blogbackend.model.dto.UserDTO;
import com.hoquangnam.blogbackend.model.persistence.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserDTO map(UserEntity userEntity);

    @Mapping(target = "createdDate", ignore = true)
    @Mapping(target = "updatedDate", ignore = true)
    @Mapping(target = "lastModifiedDate", ignore = true)
    @Mapping(target = "deletedDate", ignore = true)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "roles", ignore = true)
    @Mapping(target = "activatedDate", ignore = true)
    @Mapping(target = "followers", ignore = true)
    @Mapping(target = "readBlogs", ignore = true)
    UserEntity map(AuthDTO request);
    UserDTO map(UserDetailsImpl userDetails);

    BlogDocument.UserDocument mapToDocument(UserEntity entity);
}