package com.hoquangnam.blogbackend.core.mapper;

import com.hoquangnam.blogbackend.core.search.document.BlogDocument;
import com.hoquangnam.blogbackend.model.dto.BlogDTO;
import com.hoquangnam.blogbackend.model.persistence.BlogEntity;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(
        componentModel = "spring",
        uses = { UserMapper.class, TagMapper.class },
        injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
public interface BlogMapper {
    BlogDTO map(BlogEntity blogEntity);
    @Mapping(target = "createdDate", ignore = true)
    @Mapping(target = "updatedDate", ignore = true)
    @Mapping(target = "lastModifiedDate", ignore = true)
    @Mapping(target = "deletedDate", ignore = true)
    @Mapping(target = "tags", ignore = true)
    @Mapping(target = "authoredBy", ignore = true)
    BlogEntity map(BlogDTO blogDTO);
    BlogDocument mapToDocument(BlogEntity entity);
}