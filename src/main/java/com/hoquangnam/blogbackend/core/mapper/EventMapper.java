package com.hoquangnam.blogbackend.core.mapper;

import com.hoquangnam.blogbackend.core.event.OutboxEvent;
import com.hoquangnam.blogbackend.model.persistence.OutboxEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface EventMapper {
    OutboxEntity map(OutboxEvent event);
}