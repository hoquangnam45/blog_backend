package com.hoquangnam.blogbackend.core.mapper;

import com.hoquangnam.blogbackend.core.search.document.BlogDocument;
import com.hoquangnam.blogbackend.model.dto.TagDTO;
import com.hoquangnam.blogbackend.model.persistence.TagEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TagMapper {
    TagDTO map(TagEntity tagEntity);
    List<TagDTO> mapEntities(List<TagEntity> tagEntities);

    BlogDocument.TagDocument mapToDocument(TagEntity entity);
}