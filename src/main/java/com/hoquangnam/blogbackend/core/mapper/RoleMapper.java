package com.hoquangnam.blogbackend.core.mapper;

import com.hoquangnam.blogbackend.constant.Role;
import com.hoquangnam.blogbackend.model.persistence.RoleEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public class RoleMapper {
    public Role map(RoleEntity roleEntity) {
        return Role.fromValue(roleEntity.getId());
    }
}