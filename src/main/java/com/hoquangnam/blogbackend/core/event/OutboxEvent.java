package com.hoquangnam.blogbackend.core.event;

import java.util.Map;

/**
 * Reference:
 * <a href="https://debezium.io/blog/2019/02/19/reliable-microservices-data-exchange-with-the-outbox-pattern/">...</a>
 * <a href="https://debezium.io/documentation/reference/0.9/configuration/outbox-event-router.html">...</a>
 */
public interface OutboxEvent {
    String getId();
    String getAggregateType();
    String getAggregateId();
    String getType();
    Map<String, ?> getPayload();
}