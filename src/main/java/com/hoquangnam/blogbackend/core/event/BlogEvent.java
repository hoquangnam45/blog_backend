package com.hoquangnam.blogbackend.core.event;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hoquangnam.blogbackend.constant.BlogEventType;
import com.hoquangnam.blogbackend.core.search.document.BlogDocument;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.util.Map;
import java.util.UUID;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BlogEvent implements OutboxEvent {
    private static final String AGGREGATE_TYPE = "Blog";
    private BlogEventType type;
    private Map<String, ?> payload;
    private UUID id;
    private UUID docId;

    public static BlogEvent ofCreate(BlogDocument doc) {
        var objectMapper = new ObjectMapper();
        return BlogEvent.builder()
                .docId(doc.getId())
                .payload(objectMapper.convertValue(doc, new TypeReference<>() {}))
                .type(BlogEventType.CREATED)
                .id(UUID.randomUUID())
                .build();
    }

    public static BlogEvent ofUpdate(BlogDocument doc) {
        var objectMapper = new ObjectMapper();
        return BlogEvent.builder()
                .docId(doc.getId())
                .payload(objectMapper.convertValue(doc, new TypeReference<>() {}))
                .type(BlogEventType.CREATED)
                .id(UUID.randomUUID())
                .build();
    }

    public static BlogEvent ofDelete() {
        return BlogEvent.builder()
                .payload(null)
                .type(BlogEventType.DELETED)
                .id(UUID.randomUUID())
                .build();
    }


    @Override
    public String getAggregateType() {
        return AGGREGATE_TYPE;
    }

    @Override
    public String getAggregateId() {
        return docId.toString();
    }

    @Override
    public Map<String, ?> getPayload() {
        return payload;
    }

    @Override
    public String getType() {
        return type.name();
    }

    @Override
    public String getId() {
        return id.toString();
    }
}