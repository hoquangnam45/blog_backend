package com.hoquangnam.blogbackend.core.event;

import com.hoquangnam.blogbackend.core.mapper.EventMapper;
import com.hoquangnam.blogbackend.repository.OutboxRepository;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * Reference:
 *  Implement with outbox pattern: http://www.kamilgrzybek.com/design/the-outbox-pattern/
 *  Read your write consistency: https://arpitbhayani.me/blogs/read-your-write-consistency
 */
@Component
public class EventListenerHandler {
    private final OutboxRepository outboxRepository;
    private final EventMapper eventMapper;

    EventListenerHandler(OutboxRepository outboxRepository, EventMapper eventMapper) {
        this.outboxRepository = outboxRepository;
        this.eventMapper = eventMapper;
    }

    @EventListener
    public void receive(OutboxEvent event) {
        var entity = eventMapper.map(event);
        outboxRepository.save(entity);
        outboxRepository.delete(entity);
    }
}