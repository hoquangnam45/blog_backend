package com.hoquangnam.blogbackend.repository;

import com.hoquangnam.blogbackend.model.persistence.CommentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;
import java.util.UUID;

@Repository
public interface CommentRepositoryBasic extends JpaRepository<CommentEntity, UUID> {
    Set<CommentEntity> findAllByBlog_Id(UUID blogId);
}