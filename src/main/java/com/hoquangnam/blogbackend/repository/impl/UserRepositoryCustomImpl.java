package com.hoquangnam.blogbackend.repository.impl;

import com.hoquangnam.blogbackend.core.exception.ResourceNotFoundException;
import com.hoquangnam.blogbackend.model.persistence.UserEntity;
import com.hoquangnam.blogbackend.repository.UserRepositoryBasic;
import com.hoquangnam.blogbackend.repository.UserRepositoryCustom;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;

@Component
public class UserRepositoryCustomImpl implements UserRepositoryCustom {
    private final UserRepositoryBasic userRepository;

    UserRepositoryCustomImpl(UserRepositoryBasic userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserEntity getByUsername(String username) throws ResourceNotFoundException {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new ResourceNotFoundException(MessageFormat.format("User not found with username {0}", username)));
    }
}