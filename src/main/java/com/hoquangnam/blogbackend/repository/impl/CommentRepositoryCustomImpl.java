package com.hoquangnam.blogbackend.repository.impl;

import com.hoquangnam.blogbackend.core.exception.ResourceNotFoundException;
import com.hoquangnam.blogbackend.model.persistence.CommentEntity;
import com.hoquangnam.blogbackend.repository.CommentRepositoryBasic;
import com.hoquangnam.blogbackend.repository.CommentRepositoryCustom;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.util.Optional;
import java.util.UUID;

@Component
public class CommentRepositoryCustomImpl implements CommentRepositoryCustom {
    private final CommentRepositoryBasic commentRepository;

    CommentRepositoryCustomImpl(CommentRepositoryBasic commentRepository) {
        this.commentRepository = commentRepository;
    }

    @Override
    public CommentEntity getReplyTo(UUID commentId) throws ResourceNotFoundException {
        return Optional.ofNullable(commentId)
                .flatMap(commentRepository::findById)
                .map(CommentEntity::getReplyTo)
                .orElseThrow(() -> new ResourceNotFoundException(MessageFormat.format("Can't find parent comment of reply id {0}", commentId)));
    }

    @Override
    public CommentEntity getCommentEntity(UUID commentId) throws ResourceNotFoundException {
        return Optional.ofNullable(commentId)
                .flatMap(commentRepository::findById)
                .orElseThrow(() -> new ResourceNotFoundException(MessageFormat.format("Comment with id {0} is not existed", commentId)));
    }
}