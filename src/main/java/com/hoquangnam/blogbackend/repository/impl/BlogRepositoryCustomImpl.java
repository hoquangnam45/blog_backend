package com.hoquangnam.blogbackend.repository.impl;

import com.hoquangnam.blogbackend.core.exception.ResourceNotFoundException;
import com.hoquangnam.blogbackend.model.persistence.BlogEntity;
import com.hoquangnam.blogbackend.repository.BlogRepositoryBasic;
import com.hoquangnam.blogbackend.repository.BlogRepositoryCustom;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.util.UUID;

@Component
public class BlogRepositoryCustomImpl implements BlogRepositoryCustom {
    private final BlogRepositoryBasic blogRepository;

    BlogRepositoryCustomImpl(BlogRepositoryBasic blogRepository) {
        this.blogRepository = blogRepository;
    }

    @Override
    public BlogEntity getBlogEntity(UUID blogId) throws ResourceNotFoundException {
        return blogRepository.findById(blogId)
                .orElseThrow(() -> new ResourceNotFoundException(MessageFormat.format("Blog with id {0} is not existed or had been deleted", blogId)));
    }
}