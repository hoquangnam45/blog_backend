package com.hoquangnam.blogbackend.repository.impl;

import com.hoquangnam.blogbackend.core.exception.ResourceNotFoundException;
import com.hoquangnam.blogbackend.model.persistence.TagEntity;
import com.hoquangnam.blogbackend.repository.TagRepositoryBasic;
import com.hoquangnam.blogbackend.repository.TagRepositoryCustom;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;

@Component
public class TagRepositoryCustomImpl implements TagRepositoryCustom {
    private final TagRepositoryBasic tagRepository;

    TagRepositoryCustomImpl(TagRepositoryBasic tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public TagEntity getTagEntity(String keyword) throws ResourceNotFoundException {
        return tagRepository.findByKeyword(keyword)
                .orElseThrow(() -> new ResourceNotFoundException(MessageFormat.format("Tag with keyword {0} is not existed or had been deleted", keyword)));
    }
}