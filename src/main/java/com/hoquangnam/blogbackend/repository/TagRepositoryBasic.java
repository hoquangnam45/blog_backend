package com.hoquangnam.blogbackend.repository;

import com.hoquangnam.blogbackend.model.persistence.TagEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface TagRepositoryBasic extends JpaRepository<TagEntity, UUID> {
    Optional<TagEntity> findByKeyword(String keyword);
    boolean existsByKeyword(String keyword);
    Page<TagEntity> findAllByKeywordStartingWith(String keyword, Pageable pageable);
    List<TagEntity> findAllByKeywordStartingWith(String keyword);
    List<TagEntity> findAllByKeyword(String keyword);

    @Query(
            value = "SELECT t FROM TagEntity t ORDER BY SIZE(t.blogs) ASC",
            countQuery = "SELECT COUNT(t) FROM TagEntity t")
    Page<TagEntity> findMostUsedTags(Pageable page);
}