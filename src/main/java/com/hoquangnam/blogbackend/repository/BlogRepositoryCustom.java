package com.hoquangnam.blogbackend.repository;

import com.hoquangnam.blogbackend.core.exception.ResourceNotFoundException;
import com.hoquangnam.blogbackend.model.persistence.BlogEntity;

import java.util.UUID;

/**
 * Reference: Custom method with spring data repository
 *  https://stackoverflow.com/questions/11880924/how-to-add-custom-method-to-spring-data-jpa
 *  https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories.single-repository-behavior
 */
public interface BlogRepositoryCustom {
    BlogEntity getBlogEntity(UUID id) throws ResourceNotFoundException;
}