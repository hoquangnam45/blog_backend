package com.hoquangnam.blogbackend.repository;

import com.hoquangnam.blogbackend.model.persistence.BlogEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface BlogRepositoryBasic extends JpaRepository<BlogEntity, UUID> {}