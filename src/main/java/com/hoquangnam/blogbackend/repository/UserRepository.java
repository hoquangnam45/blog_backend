package com.hoquangnam.blogbackend.repository;

import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends UserRepositoryBasic, UserRepositoryCustom {}