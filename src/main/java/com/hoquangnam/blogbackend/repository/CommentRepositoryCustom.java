package com.hoquangnam.blogbackend.repository;

import com.hoquangnam.blogbackend.core.exception.ResourceNotFoundException;
import com.hoquangnam.blogbackend.model.persistence.CommentEntity;

import java.util.UUID;

public interface CommentRepositoryCustom {
    CommentEntity getReplyTo(UUID commentId) throws ResourceNotFoundException;
    CommentEntity getCommentEntity(UUID commentId) throws ResourceNotFoundException;
}