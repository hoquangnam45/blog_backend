package com.hoquangnam.blogbackend.repository;

import org.springframework.stereotype.Repository;

@Repository
public interface TagRepository extends TagRepositoryCustom, TagRepositoryBasic {}