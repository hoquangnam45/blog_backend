package com.hoquangnam.blogbackend.repository;

import org.springframework.stereotype.Repository;

@Repository
public interface BlogRepository extends BlogRepositoryBasic, BlogRepositoryCustom {}