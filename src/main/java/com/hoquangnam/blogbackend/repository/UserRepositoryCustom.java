package com.hoquangnam.blogbackend.repository;

import com.hoquangnam.blogbackend.core.exception.ResourceNotFoundException;
import com.hoquangnam.blogbackend.model.persistence.UserEntity;

public interface UserRepositoryCustom {
    UserEntity getByUsername(String username) throws ResourceNotFoundException;
}