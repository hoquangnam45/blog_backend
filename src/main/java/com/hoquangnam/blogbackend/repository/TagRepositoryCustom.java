package com.hoquangnam.blogbackend.repository;

import com.hoquangnam.blogbackend.core.exception.ResourceNotFoundException;
import com.hoquangnam.blogbackend.model.persistence.TagEntity;

public interface TagRepositoryCustom {
    TagEntity getTagEntity(String keyword) throws ResourceNotFoundException;
}