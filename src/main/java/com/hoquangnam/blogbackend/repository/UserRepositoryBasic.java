package com.hoquangnam.blogbackend.repository;

import com.hoquangnam.blogbackend.model.persistence.UserEntity;
import lombok.NonNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepositoryBasic extends JpaRepository<UserEntity, UUID> {
    Optional<UserEntity> findByUsername(String username);
    @NonNull
    Optional<UserEntity> findById(@NonNull UUID id);
    Boolean existsByUsername(String username);
    Boolean existsByEmail(String email);
}