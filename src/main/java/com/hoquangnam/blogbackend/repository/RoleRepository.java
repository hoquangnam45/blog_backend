package com.hoquangnam.blogbackend.repository;

import com.hoquangnam.blogbackend.model.persistence.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<RoleEntity, Integer> {}