package com.hoquangnam.blogbackend.repository;

import com.hoquangnam.blogbackend.model.persistence.OutboxEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface OutboxRepository extends JpaRepository<OutboxEntity, UUID> {}