package com.hoquangnam.blogbackend.repository;

import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends CommentRepositoryCustom, CommentRepositoryBasic {}