#!/bin/sh

setup_connector() {
  CURRENT_SCRIPT_DIR=$(dirname "$0")
  curl -X POST http://localhost:8083/connectors \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d @"$CURRENT_SCRIPT_DIR"/connect_configs/postgresql-blog-connector.json
  curl -X POST http://localhost:8083/connectors \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -d @"$CURRENT_SCRIPT_DIR"/connect_configs/elasticsearch-blog-sink.json
}