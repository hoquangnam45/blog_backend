CREATE VIEW blog_user_active AS
SELECT * FROM blog_user WHERE blog_user.deletedDate IS NULL;

CREATE VIEW blog_active AS
SELECT * FROM blog WHERE blog.deletedDate IS NULL;

CREATE VIEW comment_active AS
SELECT * FROM comment WHERE comment.deletedDate IS NULL;

CREATE VIEW tag_active AS
SELECT * from tag WHERE tag.deletedDate IS NULL;