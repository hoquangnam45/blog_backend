#!/bin/sh

# Not use pwd so this can be exec even when not in the script directory
CURRENT_SCRIPT_DIR=$( dirname -- "$( readlink -f -- "$0"; )"; )
# DB Migration postgres
docker pull flyway/flyway:latest-alpine
docker run \
      --network="host" \
      --mount type=bind,source="$CURRENT_SCRIPT_DIR"/postgres/db_migration,target=/flyway/sql \
      --rm flyway/flyway:latest-alpine \
     -url="jdbc:postgresql://localhost:5432/blog_db" \
     -user="docker" \
     -password="1234" \
     migrate