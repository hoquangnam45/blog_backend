#!/bin/sh
# Not use pwd so this can be exec even when not in the script directory
CURRENT_SCRIPT_DIR=$( dirname -- "$( readlink -f -- "$0"; )"; )

# Setup docker services
docker-compose -f "$CURRENT_SCRIPT_DIR"/cache/docker-compose.yml up -d

docker-compose -f "$CURRENT_SCRIPT_DIR"/postgres/docker-compose.yml up -d
chmod +x ./db_migration.sh
./db_migration.sh

docker-compose -f "$CURRENT_SCRIPT_DIR"/kafka/docker-compose.yml up -d
docker-compose -f "$CURRENT_SCRIPT_DIR"/elk/docker-compose.yml up -d

# Setup connector and source to postgres and elastic search
docker-compose -f "$CURRENT_SCRIPT_DIR"/debezium/docker-compose.yml up -d