ARG BUILD_IMG=gradle
ARG BUILD_IMG_TAG=7.4.2-jdk18-alpine

# Build spring project
FROM $BUILD_IMG:$BUILD_IMG_TAG AS cache-dependencies
WORKDIR /tmp
# Order build step to ultilize docker cache to reduce build time
COPY build.gradle gradlew ./
COPY gradle gradle
RUN gradle resolveAllDependencies && rm -r *

FROM cache-dependencies AS build
WORKDIR /tmp
COPY . .
RUN gradle assemble

# Copy build target to destination
FROM openjdk:11.0.15-jre-slim as blog_backend
LABEL org.opencontainers.image.authors="hoquangnam45@gmail.com"
EXPOSE 80/tcp
EXPOSE 443/tcp
ARG BUILD_FOLDER
ARG USERNAME=blog_backend
ARG GROUPNAME=blog_backend
ARG UID=1000
ARG GID=1000
WORKDIR blog_backend
COPY --from=build /tmp/build/libs/*.jar blog_backend.jar
COPY src/src/main/resources/*yml config/
RUN chmod +x blog_backend.jar

# Entrypoint
RUN addgroup --gid $GID $GROUPNAME && adduser --ingroup $GROUPNAME --uid $UID $USERNAME
USER blog_backend
ENTRYPOINT ["java", "-jar", "./blog_backend.jar"]